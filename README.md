<!--
*** Build using the Best-README-Template.
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
![Trading Hero](banner.png "Trading Hero")
  <h3 align="center">Trading Hero App</h3>

  <p align="center">
    A Trading Simulator<br />
    <p>
    <a href="https://gitlab.gwdg.de/android-lab/trading-hero/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.gwdg.de/android-lab/trading-hero/-/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#built-with">Built With</a>
    </li>
  </ol>

</details>

### Built With

<div style="display: -ms-flexbox;     display: -webkit-flex;     display: flex;     -webkit-flex-direction: row;     -ms-flex-direction: row;     flex-direction: row;     -webkit-flex-wrap: wrap;     -ms-flex-wrap: wrap;     flex-wrap: wrap;     -webkit-justify-content: space-around;     -ms-flex-pack: distribute;     justify-content: space-around;     -webkit-align-content: stretch;     -ms-flex-line-pack: stretch;     align-content: stretch;     -webkit-align-items: flex-start;     -ms-flex-align: start;     align-items: flex-start;">
<a href="https://kotlinlang.org/"><img src="https://upload.wikimedia.org/wikipedia/commons/0/06/Kotlin_Icon.svg" alt="Kotlin" width="64" height="64" title="Kotlin"></a>
<a href="https://gradle.org/"><img src="https://gradle.org/images/gradle-knowledge-graph-logo.png" alt="Gradle" width="64" height="64" title="Gradle"></a>
<a href="https://developer.android.com//"><img src="https://cdn.worldvectorlogo.com/logos/android-logomark.svg" alt="Android" width="64" height="64" title="Android"></a>
<a href="https://developer.android.com/jetpack/compose"><img src="https://3.bp.blogspot.com/-VVp3WvJvl84/X0Vu6EjYqDI/AAAAAAAAPjU/ZOMKiUlgfg8ok8DY8Hc-ocOvGdB0z86AgCLcBGAsYHQ/s1600/jetpack%2Bcompose%2Bicon_RGB.png" alt="Jetpack Compose" width="64" height="64" title="Jetpack Compose"></a>
</div>
