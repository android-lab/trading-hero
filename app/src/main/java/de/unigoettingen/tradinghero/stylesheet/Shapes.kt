package de.unigoettingen.tradinghero.stylesheet

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

val Shapes = Shapes(
    small = RoundedCornerShape(50),
    medium = RoundedCornerShape(24.dp),
    large = RoundedCornerShape(8.dp)
)
