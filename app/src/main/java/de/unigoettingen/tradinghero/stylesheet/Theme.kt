package de.unigoettingen.tradinghero.stylesheet

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import de.unigoettingen.tradinghero.viewmodel.SettingsViewModel

val darkColors = darkColors(
    primary = Color(0xFFFFFFFF),
    primaryVariant = Color(0xFFF1EBEB),
    secondary = Color(0xFF455A64),
    secondaryVariant = Color(0xFFC3C7C9),
    error = Color(0xFFF81E1E),
    surface = Color(0xFF2B2D2E),
    background = Color(0xff18191a)
)

val lightColors = lightColors(
    primary = Color(0xFF455A64),
    primaryVariant = Color(0xFF607D8B),
    secondary = Color(0xFF83AFC4),
    secondaryVariant = Color(0xFF000000),
    error = Color(0xFFF81E1E),
    surface = Color(0xFFA5BCC4),
    background = Color(0xFFCFD8DC)
)

val splitColor = Color(0, 0, 170)
val positiveChangeColor = Color(0, 170, 0)
val negativeChangeColor = Color(170, 0, 0)

@Composable
fun Theme(settingsViewModel: SettingsViewModel = hiltViewModel(), content: @Composable () -> Unit) {
    val isDarkMode by settingsViewModel.isDarkModeEnabled.collectAsState(true)

    MaterialTheme(
        if (isDarkMode) darkColors else lightColors,
        TypographyStyling,
        Shapes,
        content = content
    )
}
