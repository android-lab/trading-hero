package de.unigoettingen.tradinghero

object Configuration {
    const val CLIENT_ID = "103555545578-k00sr85i916r5udg7kjnt6fqipapplvt.apps.googleusercontent.com"
    const val API_URL = "https://api.tradinghero.srtf.dev"
    const val FRIEND_INVITATION_URI = "https://tradinghero.srtf.dev/friends/%s"
    const val WEB_FRIEND_INVITATION_URI = "th://tradinghero.srtf.dev/friends/%s"
    const val RETROFIT_LOGGING = false
}
