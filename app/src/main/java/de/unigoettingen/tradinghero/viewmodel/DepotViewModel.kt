package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.CourseRepository
import de.unigoettingen.tradinghero.api.DepotRepository
import de.unigoettingen.tradinghero.api.StockRepository
import de.unigoettingen.tradinghero.model.Balance
import de.unigoettingen.tradinghero.model.DepotScreenEntry
import de.unigoettingen.tradinghero.model.DepotScreenState
import de.unigoettingen.tradinghero.util.retryOnTimeout
import io.reactivex.rxjava3.core.Observable
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DepotViewModel @Inject constructor(
    private val depotRepository: DepotRepository,
    private val stockRepository: StockRepository,
    private val courseRepository: CourseRepository
) :
    ViewModel() {

    private val stocks: MutableMap<String, DepotScreenEntry> = mutableMapOf()

    fun loadDepot(currency: String?) {
        viewModelScope.launch {
            depotRepository
                .getDepot()
                .retryOnTimeout()
                .doOnError { println("Error ${it.message}") }
                .onErrorComplete()
                .subscribe { depot ->
                    depot.entries.forEach {
                        stocks[it.symbol] = DepotScreenEntry(amount = it.amount)
                    }

                    updateState()

                    depot.entries.forEach { depotEntry ->
                        Observable.zip(
                            stockRepository.getStockInformation(depotEntry.symbol, currency)
                                .retryOnTimeout()
                                .doOnError { println("Error ${it.message}") }
                                .onErrorComplete(),
                            courseRepository.getCurrentPrice(depotEntry.symbol, currency)
                                .retryOnTimeout()
                                .doOnError { println("Error ${it.message}") }
                                .onErrorComplete()
                        ) { stock, course ->
                            stocks[stock.symbol] =
                                stocks[stock.symbol]?.copy(
                                    stock = stock,
                                    currentPrice = course?.price
                                ) ?: DepotScreenEntry(
                                    stock = stock,
                                    currentPrice = course?.price
                                )
                            updateState()
                        }.subscribe()
                    }
                }
        }
    }

    fun loadBalance(currency: String?) {
        depotRepository.getBalance(currency)
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe { balanceView ->
                balance.value = balanceView
            }
    }

    private fun updateState() {
        viewModelScope.launch {
            depot.emit(DepotScreenState(stocks.toMap()))
        }
    }

    val depot = MutableStateFlow(DepotScreenState(null))

    val balance = MutableStateFlow<Balance?>(null)
}
