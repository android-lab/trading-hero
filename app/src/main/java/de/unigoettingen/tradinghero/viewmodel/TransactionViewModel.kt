package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.TransactionRepository
import de.unigoettingen.tradinghero.model.Transaction
import de.unigoettingen.tradinghero.model.TransactionWrite
import de.unigoettingen.tradinghero.util.retryOnTimeout
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(private val transactionRepository: TransactionRepository) :
    ViewModel() {

    val transactions = MutableStateFlow<List<Transaction>>(listOf())

    fun makeTransaction(
        transaction: TransactionWrite,
        shouldRetryOnTimeout: Boolean = true,
        onError: (Throwable) -> Unit = {},
        onComplete: () -> Unit = {}
    ) {
        transactionRepository
            .makeTransaction(transaction)
            .retryOnTimeout(shouldRetryOnTimeout)
            .doOnError { onError(it) }
            .onErrorComplete()
            .subscribe {
                onComplete()
            }
    }

    fun loadTransactionHistory(
        symbol: String,
        shouldRetryOnTimeout: Boolean = true,
        onError: (Throwable) -> Unit = {},
        onComplete: () -> Unit = {}
    ) {
        transactionRepository
            .getTransactionHistory(symbol)
            .retryOnTimeout(shouldRetryOnTimeout)
            .doOnError { onError(it) }
            .onErrorComplete()
            .subscribe { data ->
                transactions.value = data.transactions
                onComplete()
            }
    }
}
