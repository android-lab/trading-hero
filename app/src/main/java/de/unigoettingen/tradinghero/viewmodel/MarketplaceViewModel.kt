package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.RatingRepository
import de.unigoettingen.tradinghero.api.StockRepository
import de.unigoettingen.tradinghero.model.Rating
import de.unigoettingen.tradinghero.model.RatingListEntry
import de.unigoettingen.tradinghero.model.RatingType
import de.unigoettingen.tradinghero.model.Stock
import de.unigoettingen.tradinghero.util.retryOnTimeout
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class MarketplaceViewModel @Inject constructor(
    private val ratingRepository: RatingRepository,
    private val stockRepository: StockRepository,
) : ViewModel() {
    val socialRatings = MutableStateFlow<List<RatingListEntry>>(listOf())
    val marketCapRatings = MutableStateFlow<List<RatingListEntry>>(listOf())
    val priceRatings = MutableStateFlow<List<RatingListEntry>>(listOf())

    val socialRating = MutableStateFlow<Rating?>(null)
    val marketCapRating = MutableStateFlow<Rating?>(null)
    val priceRating = MutableStateFlow<Rating?>(null)

    val socialTitle = "MOST TALKED ABOUT"
    val marketCapTitle = "HIGHEST MARKET CAP"
    val priceTitle = "HIGHEST PRICE"

    fun loadAllRatings(currency: String?) {
        RatingType.values().forEach {
            loadRatings(currency, it, 20)
        }
    }

    fun loadRatings(
        currency: String?,
        type: RatingType,
        amount: Int?,
        onComplete: () -> Unit = {}
    ) {
        loadTopRatings(currency, type, amount).subscribe { data ->
            // Receive the Pairs and put them into the state
            when (type) {
                RatingType.SOCIAL -> socialRatings.value = data.map { (rating, stock) ->
                    RatingListEntry(rating.placement, RatingType.SOCIAL, stock)
                }
                RatingType.PRICE -> priceRatings.value = data.map { (rating, stock) ->
                    RatingListEntry(rating.placement, RatingType.PRICE, stock)
                }
                RatingType.MARKET_CAP -> marketCapRatings.value = data.map { (rating, stock) ->
                    RatingListEntry(rating.placement, RatingType.MARKET_CAP, stock)
                }
            }
            onComplete()
        }
    }

    fun loadRating(type: RatingType, symbol: String) {
        ratingRepository
            .getPosition(type, symbol)
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                when (type) {
                    RatingType.SOCIAL -> socialRating.value = it
                    RatingType.PRICE -> priceRating.value = it
                    RatingType.MARKET_CAP -> marketCapRating.value = it
                }
            }
    }

    private fun loadTopRatings(
        currency: String?,
        type: RatingType,
        amount: Int?
    ): Single<List<Pair<Rating, Stock>>> {
        return ratingRepository
            .getTop(type, amount)
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .processRatingsAndLoadStocks(currency)
    }

    private fun Observable<List<Rating>>.processRatingsAndLoadStocks(
        currency: String?
    ): Single<List<Pair<Rating, Stock>>> {
        return this.switchMap { data ->
            // Load the Stock data for each Rating
            // and then return the Pairs of Rating and Stock
            // one by one
            Observable.merge(
                data.map {
                    // Combine each Rating with the corresponding Stock
                    Observable.zip(
                        Observable.just(it).retryOnTimeout()
                            .doOnError { error -> println("Error ${error.message}") }
                            .onErrorComplete(),
                        // Load the Stock
                        stockRepository.getStockInformation(
                            it.stock,
                            currency
                        ).retryOnTimeout()
                            .doOnError { error -> println("Error ${error.message}") }
                            .onErrorComplete()

                    ) { rating, stock ->
                        // Pair the concrete values from the observables above into a Pair
                        // and return that Pair to the merge Operator
                        Pair(rating, stock)
                    }
                }
            )
        }
            // Put all the emitted Pairs back together and sort
            .toSortedList(compareBy { it.first.placement })
    }
}
