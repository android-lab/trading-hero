package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.AccountRepository
import de.unigoettingen.tradinghero.model.Person
import de.unigoettingen.tradinghero.model.PersonWriteView
import de.unigoettingen.tradinghero.util.retryOnTimeout
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class PersonViewModel @Inject constructor(private val accountRepository: AccountRepository) :
    ViewModel() {

    fun loadPerson(
        shouldRetryOnTimeout: Boolean = true,
        onError: (Throwable) -> Unit = {},
        onComplete: (Person) -> Unit = {}
    ) {
        accountRepository
            .getPerson()
            .retryOnTimeout(shouldRetryOnTimeout)
            .doOnError { onError(it) }
            .onErrorComplete()
            .subscribe {
                onComplete(it)
                person.value = it
            }
    }

    fun updatePerson(
        personWriteView: PersonWriteView,
        shouldRetryOnTimeout: Boolean = true,
        onError: (Throwable) -> Unit = {},
        onComplete: (Person) -> Unit = {}
    ) {
        accountRepository
            .updatePerson(personWriteView)
            .retryOnTimeout(shouldRetryOnTimeout)
            .doOnError { onError(it) }
            .onErrorComplete()
            .subscribe {
                onComplete(it)
                person.value = it
            }
    }

    val person: MutableStateFlow<Person?> = MutableStateFlow(null)
}
