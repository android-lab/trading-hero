package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.StockRepository
import de.unigoettingen.tradinghero.model.Stock
import de.unigoettingen.tradinghero.util.retryOnTimeout
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class StockViewModel @Inject constructor(private val stockRepository: StockRepository) :
    ViewModel() {
    val stock = MutableStateFlow<Stock?>(null)

    fun getStockInformation(symbol: String, currency: String?) {
        stockRepository
            .getStockInformation(symbol, currency)
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                stock.value = it
            }
    }
}
