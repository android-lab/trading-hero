package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.AccountRepository
import de.unigoettingen.tradinghero.api.DepotRepository
import de.unigoettingen.tradinghero.api.FriendRepository
import de.unigoettingen.tradinghero.model.FriendEntry
import de.unigoettingen.tradinghero.model.FriendRequest
import de.unigoettingen.tradinghero.model.FriendShip
import de.unigoettingen.tradinghero.model.Person
import de.unigoettingen.tradinghero.util.retryOnTimeout
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class FriendListViewModel @Inject constructor(
    private val accountRepository: AccountRepository,
    private val depotRepository: DepotRepository,
    private val friendRepository: FriendRepository
) : ViewModel() {
    val friends = MutableStateFlow<List<FriendEntry>?>(null)
    val invites = MutableStateFlow<List<FriendEntry>>(emptyList())
    val requests = MutableStateFlow<List<FriendEntry>?>(null)
    val friend = MutableStateFlow<Person?>(null)

    fun loadFriends(currency: String?, onComplete: () -> Unit = {}) {
        friendRepository.getFriends()
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                friends.value = getFriendEntriesOfFriendships(it, currency)
                onComplete()
            }
    }

    fun loadInvites(onComplete: () -> Unit = {}) {
        friendRepository.getInvites()
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                invites.value = getFriendEntriesOfRequests(it)
                onComplete()
            }
    }

    fun loadRequests(onComplete: () -> Unit = {}) {
        friendRepository.getRequests()
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe { friendRequestViews ->
                requests.value = getFriendEntriesOfRequests(friendRequestViews)
                onComplete()
            }
    }

    fun getFriend(friendId: String, onComplete: (Person?) -> Unit = {}) {
        accountRepository.getPerson(friendId)
            .retryOnTimeout()
            .doOnError { onComplete(null) }
            .onErrorComplete()
            .subscribe {
                friend.value = it
                onComplete(it)
            }
    }

    fun addFriend(friendId: String, onComplete: () -> Unit = {}) {
        friendRepository.addFriend(friendId)
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                if (invites.value.any { it.id == friendId }) {
                    loadInvites(onComplete)
                } else {
                    loadRequests(onComplete)
                }
            }
    }

    fun removeFriend(friendId: String, onComplete: () -> Unit = {}) {
        friendRepository.removeFriend(friendId)
            .doOnError { println("Error: $it") }
            .onErrorComplete()
            .subscribe { onComplete() }
    }

    private fun getFriendEntriesOfFriendships(
        friendShips: List<FriendShip>,
        currency: String?
    ): List<FriendEntry> {
        return friendShips.map { request ->
            val balance = depotRepository.getDepotBalance(request.friendId, currency)
                .retryOnTimeout()
                .doOnError { println("Error ${it.message}") }
                .onErrorComplete()
                .blockingFirst()

            with(
                accountRepository.getPerson(request.friendId)
                    .retryOnTimeout()
                    .doOnError { println("Error ${it.message}") }
                    .onErrorComplete()
                    .blockingFirst()
            ) {
                FriendEntry(request.friendId, name, image, balance.amount)
            }
        }
    }

    private fun getFriendEntriesOfRequests(friendRequests: List<FriendRequest>) =
        friendRequests.map { request ->
            with(
                accountRepository.getPerson(request.friendId)
                    .retryOnTimeout()
                    .doOnError { println("Error ${it.message}") }
                    .onErrorComplete()
                    .blockingFirst()
            ) {
                FriendEntry(request.friendId, name, image)
            }
        }.toMutableList()
}
