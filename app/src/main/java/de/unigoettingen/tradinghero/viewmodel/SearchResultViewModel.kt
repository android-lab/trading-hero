package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.StockSearchRepository
import de.unigoettingen.tradinghero.model.TinyStock
import de.unigoettingen.tradinghero.util.retryOnTimeout
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class SearchResultViewModel @Inject constructor(private val stockSearchRepository: StockSearchRepository) :
    ViewModel() {
    fun searchStock(filter: String, includePrice: Boolean) {
        stockSearchRepository
            .searchStock(filter, includePrice)
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe { search.value = it }
    }

    val search = MutableStateFlow<List<TinyStock>>(listOf())
}
