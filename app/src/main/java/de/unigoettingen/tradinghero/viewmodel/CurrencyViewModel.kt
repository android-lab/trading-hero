package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.CurrencyRepository
import de.unigoettingen.tradinghero.util.retryOnTimeout
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class CurrencyViewModel @Inject constructor(private val currencyRepository: CurrencyRepository) :
    ViewModel() {
    val currencyView = MutableStateFlow<Set<String>>(setOf())

    fun loadCurrencies() {
        currencyRepository
            .getListOfCurrencies()
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                currencyView.value = it
            }
    }
}
