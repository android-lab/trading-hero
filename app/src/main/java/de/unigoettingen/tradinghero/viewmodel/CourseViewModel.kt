package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.CourseRepository
import de.unigoettingen.tradinghero.model.Course
import de.unigoettingen.tradinghero.util.retryOnTimeout
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class CourseViewModel @Inject constructor(private val courseRepository: CourseRepository) :
    ViewModel() {
    val course = MutableStateFlow<List<Course>>(listOf())
    var lastSockCallDisposable: Disposable? = null

    fun getCourse(
        symbol: String,
        greaterDiffFromNow: Long? = null,
        lesserDiffFromNow: Long? = null,
        currency: String?
    ) {
        lastSockCallDisposable?.let {
            if (!it.isDisposed)
                it.dispose()
        }

        lastSockCallDisposable = courseRepository.getCourse(
            symbol = symbol,
            greaterDiffFromNow = greaterDiffFromNow,
            lesserDiffFromNow = lesserDiffFromNow,
            currency = currency
        )
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                course.value = it
            }
    }

    fun resetCourseList() {
        course.value = listOf()
    }
}
