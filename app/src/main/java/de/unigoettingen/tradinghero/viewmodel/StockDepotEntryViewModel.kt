package de.unigoettingen.tradinghero.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.unigoettingen.tradinghero.api.CourseRepository
import de.unigoettingen.tradinghero.api.DepotRepository
import de.unigoettingen.tradinghero.model.DepotEntry
import de.unigoettingen.tradinghero.util.retryOnTimeout
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class StockDepotEntryViewModel @Inject constructor(
    private val depotRepository: DepotRepository,
    private val courseRepository: CourseRepository
) : ViewModel() {
    var depotScreenEntry: MutableStateFlow<DepotEntry?> = MutableStateFlow(null)

    val currentPrice: MutableStateFlow<Double?> = MutableStateFlow(null)

    fun loadDepotEntity(symbol: String) {
        depotRepository
            .getDepotEntry(symbol)
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                depotScreenEntry.value = it
            }
    }

    fun loadCurrentPrice(symbol: String, currency: String) {
        courseRepository
            .getCurrentPrice(symbol, currency)
            .retryOnTimeout()
            .doOnError { println("Error ${it.message}") }
            .onErrorComplete()
            .subscribe {
                currentPrice.value = it?.price
            }
    }
}
