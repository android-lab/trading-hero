package de.unigoettingen.tradinghero.viewmodel

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import de.unigoettingen.tradinghero.dataStore
import de.unigoettingen.tradinghero.model.FormatterType
import de.unigoettingen.tradinghero.model.Settings
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(@ApplicationContext ctx: Context) :
    ViewModel() {
    private val dataStore = ctx.dataStore

    val isDarkModeEnabled = dataStore.data.map { preferences ->
        val dataStoreKey = booleanPreferencesKey(Settings.DARK_MODE.name)
        preferences[dataStoreKey] ?: true
    }

    val currency = dataStore.data.map { preferences ->
        val dataStoreKey = stringPreferencesKey(Settings.CURRENCY.name)
        preferences[dataStoreKey] ?: "USD"
    }

    val format = dataStore.data.map { preferences ->
        val dataStoreKey = stringPreferencesKey(Settings.FORMAT.name)
        FormatterType.values().find { it.name == preferences[dataStoreKey] }
            ?: FormatterType.AMERICAN
    }

    fun setDarkMode(isEnabled: Boolean) {
        val dataStoreKey = booleanPreferencesKey(Settings.DARK_MODE.name)
        viewModelScope.launch {
            dataStore.edit { settings ->
                settings[dataStoreKey] = isEnabled
            }
        }
    }

    fun setCurrency(currency: String) {
        val dataStoreKey = stringPreferencesKey(Settings.CURRENCY.name)
        viewModelScope.launch {
            dataStore.edit { settings ->
                settings[dataStoreKey] = currency
            }
        }
    }

    fun setFormat(formatterType: FormatterType) {
        val dataStoreKey = stringPreferencesKey(Settings.FORMAT.name)
        viewModelScope.launch {
            dataStore.edit { settings ->
                settings[dataStoreKey] = formatterType.name
            }
        }
    }
}
