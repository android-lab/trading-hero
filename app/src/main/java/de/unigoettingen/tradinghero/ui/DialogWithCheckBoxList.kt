package de.unigoettingen.tradinghero.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.unigoettingen.tradinghero.screen.setting.SimpleSettingsCheckbox

@Composable
fun DialogWithCheckBoxList(
    modifier: Modifier = Modifier,
    isOpen: Boolean,
    title: String,
    values: List<String>,
    selectedValue: String,
    onSelectOption: (String?) -> Unit
) {
    if (!isOpen) {
        return
    }
    AlertDialog(
        modifier = modifier,
        onDismissRequest = { onSelectOption(null) },
        title = {
            Column {
                Text(text = title, fontSize = 20.sp)
            }
        },
        text = {
            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 15.dp)
            ) {
                items(values) { text ->
                    Spacer(modifier = Modifier.height(4.dp))
                    SimpleSettingsCheckbox(
                        modifier = Modifier.height(50.dp),
                        title = text,
                        isChecked = text == selectedValue,
                        onCheckedChange = { onSelectOption(text) }
                    )
                }
            }
        },
        buttons = {}
    )
}

@Preview
@Composable
fun DialogPreview() {
    DialogWithCheckBoxList(
        isOpen = true,
        title = "Titel",
        values = listOf("Option1", "Option2", "Option3"),
        selectedValue = "Option2",
        onSelectOption = {})
}

