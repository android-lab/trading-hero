package de.unigoettingen.tradinghero.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun HorizontalListWithTitle(
    modifier: Modifier = Modifier,
    title: String,
    onInvestigate: (() -> Unit)? = null,
    content: LazyListScope.() -> Unit
) {

    Column(
        modifier = Modifier.fillMaxWidth(),
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            // To match Icon Button Height, which has minHeight for accessibility
            modifier = Modifier.defaultMinSize(minHeight = 48.dp)
        ) {
            Text(
                modifier = Modifier.padding(start = 5.dp),
                text = title,
                fontSize = 24.sp,
                textAlign = TextAlign.Center,
            )
            if (onInvestigate != null) {
                Column(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.End
                ) {
                    IconButton(
                        onClick = { onInvestigate() }
                    ) {
                        Icon(Icons.Filled.ArrowForward, null)
                    }
                }
            }
        }
        LazyRow(modifier = modifier, content = content)
    }
}

@Preview
@Composable
fun Preview() {
    HorizontalListWithTitle(title = "TestTitle", onInvestigate = null) {
        items(5) {
            Text(text = "hallo", modifier = Modifier.padding(end = 5.dp))
            Divider()
        }
    }
}

@Preview
@Composable
fun PreviewInvestigate() {
    HorizontalListWithTitle(title = "TestTitle", onInvestigate = { }) {
        items(5) {
            Text(text = "hallo", modifier = Modifier.padding(end = 5.dp))
            Divider()
        }
    }
}
