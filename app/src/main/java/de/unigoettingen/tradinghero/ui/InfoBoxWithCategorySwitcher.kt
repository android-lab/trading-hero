package de.unigoettingen.tradinghero.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun InfoBoxWithCategorySwitcher(modifier: Modifier = Modifier, categories: List<InfoCategory>) {
    if (categories.isEmpty()) {
        return
    }

    val currentElement = remember {
        mutableStateOf(categories.first())
    }
    Card(
        modifier = modifier
            .padding(horizontal = 16.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            InfoBoxHeader(currentElement.value, categories) {
                currentElement.value = it
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth(0.9F)
                    .padding(10.dp)
            ) {
                currentElement.value.content()
            }
        }
    }
}

@Composable
fun InfoBoxHeader(
    currentCategory: InfoCategory,
    categories: List<InfoCategory>,
    onSelect: (InfoCategory) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.surface)
            .padding(10.dp),
        horizontalArrangement = Arrangement.SpaceEvenly,
    ) {
        categories.forEach { category ->
            if (currentCategory == category) {
                Text(
                    text = category.title,
                    style = TextStyle(
                        fontWeight = FontWeight.Bold,
                        color = MaterialTheme.colors.primaryVariant
                    ),
                    modifier = Modifier
                        .padding(2.dp),
                    fontSize = 20.sp
                )
            } else {
                Text(
                    text = category.title,
                    style = TextStyle(fontWeight = FontWeight.Normal),
                    modifier = Modifier
                        .padding(2.dp)
                        .clickable(
                            indication = null,
                            interactionSource = remember {
                                MutableInteractionSource()
                            }
                        ) { onSelect(category) }
                        .background(Color.Transparent),
                    fontSize = 20.sp
                )
            }
        }
    }
}

data class InfoCategory(
    val title: String,
    val content: @Composable () -> Unit
)

@Preview
@Composable
fun EmptyInfoPreview() {
    InfoBoxWithCategorySwitcher(categories = listOf())
}

@Preview
@Composable
fun InfoPreview() {
    InfoBoxWithCategorySwitcher(categories = listOf(InfoCategory("Titel") {
        Text("Informationen")
    }))
}

@Preview
@Composable
fun LongInfoPreview() {
    InfoBoxWithCategorySwitcher(categories = listOf(InfoCategory("Titel") {
        Text("Informationen")
        Text("Informationen")
        Text("Informationen")
        Text("Informationen")
        Text("Informationen")
        Text("Informationen")
        Text("Informationen")
        Text("Informationen")
    }))
}
@Preview
@Composable
fun CategoryPreview() {
    InfoBoxWithCategorySwitcher(categories = listOf(
        InfoCategory("Titel") { Text("Informationen") },
        InfoCategory("Titel") { Text("Informationen") },
        InfoCategory("Titel") { Text("Informationen") }
    ))
}

