package de.unigoettingen.tradinghero.ui

import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountBalanceWallet
import androidx.compose.material.icons.rounded.Group
import androidx.compose.material.icons.rounded.ShoppingCart
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NavController
import de.unigoettingen.tradinghero.routing.Routes

@Composable
fun BottomNavigationBar(navController: NavController) {
    val items = listOf(
        BottomNavigationItem.Depot,
        BottomNavigationItem.Marketplace,
        BottomNavigationItem.Friends
    )
    BottomNavigation {
        items.forEach {
            BottomNavigationItem(
                selected = navController.currentBackStackEntry?.destination?.route?.contains(it.route)
                    ?: false,
                onClick = {
                    if (navController.currentBackStackEntry?.destination?.route?.contains(it.route) != true
                    ) {
                        navController.popBackStack()
                        navController.navigate(it.route)
                    }
                },
                selectedContentColor = Color.White,
                unselectedContentColor = Color.White.copy(0.4f),
                icon = { Icon(imageVector = it.image, contentDescription = null) }
            )
        }
    }
}

sealed class BottomNavigationItem(val route: String, val name: String, val image: ImageVector) {
    object Depot : BottomNavigationItem(Routes.depot(), "Depot", Icons.Rounded.AccountBalanceWallet)
    object Marketplace :
        BottomNavigationItem(Routes.marketplace(), "Marketplace", Icons.Rounded.ShoppingCart)

    object Friends :
        BottomNavigationItem(Routes.friends(), "Friends", Icons.Rounded.Group)
}
