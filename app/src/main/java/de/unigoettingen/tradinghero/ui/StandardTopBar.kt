package de.unigoettingen.tradinghero.ui

import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.primarySurface
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun StandardTopBar(
    navigationIcon: @Composable (() -> Unit)? = null,
    text: String = "Trading Hero",
    actions: @Composable RowScope.() -> Unit = { }
) {
    TopAppBar(
        title = {
            Text(text = text)
        },
        backgroundColor = MaterialTheme.colors.primarySurface,
        navigationIcon = navigationIcon,
        actions = actions
    )
}

@Preview
@Composable
fun TopBarPreview(){
    StandardTopBar()
}
