package de.unigoettingen.tradinghero.routing

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.ExperimentalGraphicsApi
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navDeepLink
import de.unigoettingen.tradinghero.Configuration
import de.unigoettingen.tradinghero.exception.StockSymbolNotSetException
import de.unigoettingen.tradinghero.model.RatingType
import de.unigoettingen.tradinghero.model.SettingRoutes
import de.unigoettingen.tradinghero.screen.account.AccountScreen
import de.unigoettingen.tradinghero.screen.depot.DepotScreen
import de.unigoettingen.tradinghero.screen.friend.FriendScreen
import de.unigoettingen.tradinghero.screen.login.LoginScreen
import de.unigoettingen.tradinghero.screen.marketplace.MarketplaceScreen
import de.unigoettingen.tradinghero.screen.setting.SettingsScreen
import de.unigoettingen.tradinghero.screen.stock.StockScreen
import de.unigoettingen.tradinghero.screen.transaction.TransactionHistoryScreen

// The Router defines all the different routes inside the app.
// A single route consists of a string and might contain path or query params.
// These strings can then be called upon using the navController.navigate function
// inside the components to change which component is displayed.
@ExperimentalGraphicsApi
@Composable
fun Router() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Routes.login()) {
        // Duplicate login route for weird startDestination NavGraph Shenanigans,
        // where the startDestination is not allowed any arguments, even when they are nullable.
        composable(
            Routes.login(),
        ) { LoginScreen(navController) }
        composable(
            Routes.login("{route}"),
            arguments = listOf(navArgument("route") { nullable = true })
        ) {
            LoginScreen(navController, it.arguments?.getString("route"))
        }
        // Screen without Arguments
        composable(Routes.accountOverview()) { AccountScreen(navController) }
        composable(Routes.depot()) { DepotScreen(navController) }
        // Screen with path param
        composable(Routes.transactionHistory("{symbol}")) {
            TransactionHistoryScreen(
                navController,
                it.arguments?.getString("symbol") ?: throw StockSymbolNotSetException()
            )
        }
        // Screen with query param
        composable(
            Routes.friends("{friendId}"),
            arguments = listOf(navArgument("friendId") { nullable = true }),
            deepLinks = listOf(
                navDeepLink {
                    uriPattern = String.format(Configuration.FRIEND_INVITATION_URI, "{friendId}")
                },
                navDeepLink {
                    uriPattern =
                        String.format(Configuration.WEB_FRIEND_INVITATION_URI, "{friendId}")
                }
            ),
        ) {
            FriendScreen(navController = navController, it.arguments?.getString("friendId"))
        }
        composable(Routes.stock("{symbol}")) {
            StockScreen(
                navController,
                it.arguments?.getString("symbol") ?: throw StockSymbolNotSetException()
            )
        }
        composable(
            Routes.marketplace() + "?type={type}",
            arguments = listOf(navArgument("type") { nullable = true })
        ) {
            val type =
                it.arguments
                    ?.getString("type")
                    ?.let { argument -> RatingType.valueOf(argument) }
            MarketplaceScreen(
                navController,
                type
            )
        }
        composable(
            Routes.settings() + "?route={route}",
            arguments = listOf(navArgument("route") { nullable = true })
        ) {
            val route =
                it.arguments
                    ?.getString("route")
                    ?.let { argument -> SettingRoutes.valueOf(argument) }
            SettingsScreen(
                navController,
                route
            )
        }
    }
}
