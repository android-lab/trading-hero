package de.unigoettingen.tradinghero.routing

import de.unigoettingen.tradinghero.model.RatingType
import de.unigoettingen.tradinghero.model.SettingRoutes

// Routes to be used within the Router.
// All Routes should be contained here and never be accessed via plain strings.
// This allows for more flexibility in the future.
object Routes {
    fun accountOverview() = "account_overview"
    fun depot() = "depot"
    fun login(route: String? = null) =
        "login/" + if (route != null) "?route=$route" else ""

    fun marketplace(type: RatingType? = null) =
        "marketplace/" + if (type != null) "?type=${type.name}" else ""

    fun settings(route: SettingRoutes? = null) =
        "settings/" + if (route != null) "?route=${route.name}" else ""

    fun friends(friendId: String? = null) =
        "friends/" + if (friendId != null) "?friendId=${friendId}" else ""

    fun stock(symbol: String) = "stock/$symbol"
    fun transactionHistory(symbol: String) = "transaction/$symbol"
}
