package de.unigoettingen.tradinghero.model

import java.util.*

data class Person(
    val id: String,
    var name: String,
    val email: String?,
    val image: String,
    val familyName: String?,
    val givenName: String?,
    val dateCreated: Date?
)

data class PersonWriteView(
    val name: String,
    val image: String,
    val familyName: String,
    val givenName: String,
)
