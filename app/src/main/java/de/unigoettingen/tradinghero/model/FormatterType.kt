package de.unigoettingen.tradinghero.model

enum class FormatterType(val decimal: Char, val grouping: Char) {
    AMERICAN('.', ','),
    EUROPEAN(',', '.')
}
