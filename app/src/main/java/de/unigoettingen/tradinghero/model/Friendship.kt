package de.unigoettingen.tradinghero.model

data class FriendEntry(
    val id: String,
    val name: String,
    val imageUrl: String,
    val balance: Double? = null
)

data class FriendShip(
    val personId: String,
    val friendId: String,
    val dateAccepted: Long,
)

data class FriendRequest(
    val friendId: String,
    val dateCreated: Long
)
