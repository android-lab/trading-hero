package de.unigoettingen.tradinghero.model

enum class RatingType {
    SOCIAL,
    PRICE,
    MARKET_CAP
}

data class Rating(
    val placement: Int,
    val stock: String
)

data class RatingListEntry(
    val placement: Int,
    val type: RatingType,
    val stock: Stock
)