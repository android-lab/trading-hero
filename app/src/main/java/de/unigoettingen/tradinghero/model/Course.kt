package de.unigoettingen.tradinghero.model

data class Course(
    val symbol: String,
    val timeStamp: Long,
    val price: Double
)
