package de.unigoettingen.tradinghero.model

import androidx.compose.ui.graphics.Color
import java.util.concurrent.TimeUnit

data class PieChartEntry(
    val name: String,
    val startAngle: Float,
    val sweepAngle: Float,
    val color: Color
)

enum class Resolution(val duration: Long?, val displayName: String) {
    MAX(null, "max"),
    MONTH(TimeUnit.DAYS.toMillis(31), "month"),
    WEEK(TimeUnit.DAYS.toMillis(7), "week"),
    DAY(TimeUnit.DAYS.toMillis(1), "day"),
    HOUR(TimeUnit.HOURS.toMillis(1), "hour")
}
