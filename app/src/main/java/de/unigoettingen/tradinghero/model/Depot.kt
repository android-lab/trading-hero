package de.unigoettingen.tradinghero.model

data class Depot(val personId: String, val entries: List<DepotEntry>)

data class DepotEntry(val amount: Double, val symbol: String)

data class DepotListEntry(
    val name: String,
    val amount: Double,
    val price: Double
)

data class DepotScreenState(val stocks: Map<String, DepotScreenEntry>? = null)

data class DepotScreenEntry(
    val stock: Stock? = null,
    val amount: Double? = null,
    val currentPrice: Double? = null
)
