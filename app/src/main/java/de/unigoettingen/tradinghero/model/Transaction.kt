package de.unigoettingen.tradinghero.model

import java.time.OffsetDateTime

data class Transaction(
    val symbol: String,
    val amount: Double,
    val type: TransactionType,
    val price: Double,
    val date: OffsetDateTime
)

data class TransactionWrite(val symbol: String, val amount: Double, val type: TransactionType)

enum class TransactionType {
    BUY,
    SELL,
    SPLIT;
}

data class TransactionsList(
    val personId: String,
    val symbol: String,
    val transactions: List<Transaction>
)
