package de.unigoettingen.tradinghero.model

enum class Settings {
    DARK_MODE,
    CURRENCY,
    FORMAT
}

enum class SettingRoutes {
    CurrencySettings
}