package de.unigoettingen.tradinghero.model

data class Stock(
    val symbol: String,
    val currency: String,
    val name: String,
    val description: String,
    val figi: String,
    val mic: String,
    val displaySymbol: String,
    val type: String,
    val exchange: String,
    val industry: String,
    val ipo: String,
    val logo: String,
    val marketCapitalization: Double,
    val shareOutstanding: Double,
    val url: String,
)

data class TinyStock(
    val symbol: String,
    val name: String,
    val logo: String,
    val lastStockPrice: Double?,
)

fun Stock.toTinyStock(): TinyStock = TinyStock(symbol, name, logo, null)
