package de.unigoettingen.tradinghero.model

data class Balance(val personID: String, val amount: Double)
