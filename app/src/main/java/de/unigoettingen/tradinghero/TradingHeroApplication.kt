package de.unigoettingen.tradinghero

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

// Every App that uses Hilt has to define an Application
@HiltAndroidApp
class TradingHeroApplication : Application()
