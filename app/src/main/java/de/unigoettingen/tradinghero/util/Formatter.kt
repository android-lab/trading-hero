package de.unigoettingen.tradinghero.util

import de.unigoettingen.tradinghero.model.FormatterType
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

object Formatter {
    private var decimalFormat = DecimalFormat(
        "#,##0.00",
        DecimalFormatSymbols().apply {
            decimalSeparator = ','
            groupingSeparator = '.'
        }
    )

    private lateinit var currency: String

    fun setSymbols(formatterType: FormatterType) {
        decimalFormat = DecimalFormat(
            "#,##0.00",
            DecimalFormatSymbols().apply {
                this.decimalSeparator = formatterType.decimal
                this.groupingSeparator = formatterType.grouping
            }
        )
    }

    fun setCurrency(currency: String) {
        this.currency = currency
    }

    fun formatCurrency(value: Any?, multiplier: Int? = null): String {
        return decimalFormat.format(value) + (
                multiplier?.let { "E$multiplier" }
                    ?: ""
                ) + " $currency"
    }

    fun formatDecimal(value: Any?): String = decimalFormat.format(value)

    fun formatDateWithTime(value: Date): String = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault()).format(value)

    fun formatDate(value: Date): String = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(value)
}
