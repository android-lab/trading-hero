package de.unigoettingen.tradinghero.util

import io.reactivex.rxjava3.core.Observable

// Will retry the call if the call failed because of a connection problem
fun <T> Observable<T>.retryOnTimeout(
    isEnabled: Boolean = true,
): Observable<T> {
    return retry { error ->
        println(error.message)
        isEnabled && (
            error.message?.contains("failed to connect") == true ||
                error.message?.contains("timeout") == true
            )
    }
}
