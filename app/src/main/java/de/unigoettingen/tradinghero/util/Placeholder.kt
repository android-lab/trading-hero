package de.unigoettingen.tradinghero.util

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import com.google.accompanist.placeholder.PlaceholderHighlight
import com.google.accompanist.placeholder.fade
import com.google.accompanist.placeholder.placeholder

fun Modifier.customPlaceholder(
    isVisible: Boolean,
    color: Color,
    shape: Shape = RoundedCornerShape(10)
): Modifier =
    placeholder(
        visible = isVisible,
        color = color.copy(alpha = 0.6F),
        shape = shape,
        highlight = PlaceholderHighlight.fade(color.copy(alpha = 0.4F))
    )
