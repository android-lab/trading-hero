package de.unigoettingen.tradinghero.util

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ExperimentalGraphicsApi

@ExperimentalGraphicsApi
fun stringToColor(string: String): Color {
    return Color.hsl((string.hashCode() % 360).toFloat(), 1F, 0.5F)
}
