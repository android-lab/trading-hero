package de.unigoettingen.tradinghero.util

import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.awaitTouchSlopOrCancellation
import androidx.compose.foundation.gestures.drag
import androidx.compose.foundation.gestures.forEachGesture
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.pointer.PointerInputChange
import androidx.compose.ui.input.pointer.PointerInputScope
import androidx.compose.ui.input.pointer.consumePositionChange
import androidx.compose.ui.input.pointer.positionChangeConsumed
import kotlin.math.abs

// To detect both dragging and tapping with coordinates at the same time
// for the depot pie chart highlighting,
// this extends PointerInputScope by changing the code of
// PointerInputScope.detectDragGesture in DragGestureDetector.kt
suspend fun PointerInputScope.detectGestures(
    onDragStart: (Offset) -> Unit = { },
    onDragEnd: () -> Unit = { },
    onDragCancel: () -> Unit = { },
    onDrag: (change: PointerInputChange) -> Unit
) {

    forEachGesture {
        awaitPointerEventScope {

            val down = awaitFirstDown(requireUnconsumed = false)
            onDrag(down) // <-- Added
            var drag: PointerInputChange?
            // var overSlop = Offset.Zero <-- Removed as it is not needed in our case
            do {
                drag = awaitTouchSlopOrCancellation(down.id) { change, over ->
                    // Exit when fast up down movement
                    if (abs(over.x) > abs(over.y) || abs(over.y) < 5) {
                        change.consumePositionChange()
                    }
                    // overSlop = over
                }
            } while (drag != null && !drag.positionChangeConsumed())
            if (drag != null) {
                onDragStart.invoke(drag.position)
                onDrag(drag) // onDrag(drag, overSlop)
                if (
                    !drag(drag.id) {
                        onDrag(it) // onDrag(it, it.positionChange())
                        it.consumePositionChange()
                    }
                ) {
                    onDragCancel()
                } else {
                    onDragEnd()
                }
            }
            onDragEnd() // <-- Added
        }
    }
}
