package de.unigoettingen.tradinghero

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.ui.graphics.ExperimentalGraphicsApi
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.AndroidEntryPoint
import de.unigoettingen.tradinghero.routing.Router
import de.unigoettingen.tradinghero.screen.setting.InitializeSettings
import de.unigoettingen.tradinghero.stylesheet.Theme

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

// For usage of hsl in ColorHelper.kt
@ExperimentalGraphicsApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Force Portrait Orientation
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContent {
            // Does not display anything, just initializes the settings/formatters using viewmodels,
            // therefore has to be a composable
            InitializeSettings()
            // Apply Theme
            Theme {
                // Show content
                Router()
            }
        }
    }
}
