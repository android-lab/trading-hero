package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.unigoettingen.tradinghero.model.Transaction
import de.unigoettingen.tradinghero.model.TransactionWrite
import de.unigoettingen.tradinghero.model.TransactionsList
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import javax.inject.Inject

interface TransactionServiceInterface {
    @POST("/transaction")
    fun makeTransaction(@Body transaction: TransactionWrite): Observable<Transaction>

    @GET("/transaction/history/{symbol}")
    fun getTransactionHistory(
        @Path("symbol") symbol: String,
    ): Observable<TransactionsList>
}

@Module
@InstallIn(ViewModelComponent::class)
class TransactionRepository @Inject constructor(retrofit: Retrofit) {

    private val service: TransactionServiceInterface =
        retrofit.create(TransactionServiceInterface::class.java)

    fun makeTransaction(transaction: TransactionWrite): Observable<Transaction> =
        service.makeTransaction(transaction)

    fun getTransactionHistory(symbol: String): Observable<TransactionsList> =
        service.getTransactionHistory(symbol)
}
