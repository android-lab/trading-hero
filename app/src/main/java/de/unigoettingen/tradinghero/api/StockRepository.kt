package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.unigoettingen.tradinghero.model.Stock
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject

interface StockRepositoryInterface {
    @GET("/stocks/information/{symbol}")
    fun getStockInformation(
        @Path("symbol") symbol: String,
        @Query("currency") currency: String?
    ): Observable<Stock>

    @GET("/stocks/information")
    fun getStocksInformation(
        @Query("symbols") symbols: List<String>,
        @Query("currency") currency: String?
    ): Observable<List<Stock>>
}

@Module
@InstallIn(ViewModelComponent::class)
class StockRepository @Inject constructor(retrofit: Retrofit) {

    private val service: StockRepositoryInterface =
        retrofit.create(StockRepositoryInterface::class.java)

    fun getStockInformation(symbol: String, currency: String?): Observable<Stock> {
        return service.getStockInformation(symbol, currency)
    }

    fun getStocksInformation(symbols: List<String>, currency: String?): Observable<List<Stock>> {
        return service.getStocksInformation(symbols, currency)
    }
}
