package de.unigoettingen.tradinghero.api

import android.content.Context
import com.google.gson.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import de.unigoettingen.tradinghero.Configuration
import de.unigoettingen.tradinghero.exception.TokenNotInitializedException
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object API {
    var token: String? = null

    @Provides
    @Singleton
    fun getRetrofit(@ApplicationContext context: Context): Retrofit {
        val okHttpClientBuilder = OkHttpClient.Builder()
            .addInterceptor {
                if (token == null) {
                    // Maybe API Object has been destroyed, try to retrieve the token again
                    token = GoogleAuthService(context).silentLogin()?.idToken
                    if (token == null) {
                        // Token could not be retrieved, user might not be logged in at all
                        return@addInterceptor Response.Builder()
                            .code(401)
                            .request(it.request())
                            .protocol(Protocol.HTTP_2)
                            .message("Token not Initialized!")
                            .body("".toResponseBody(null))
                            .build()
                    }
                }
                // Send the request with the Auth token header set
                var response = it.proceed(
                    it.request().newBuilder().addHeader(
                        "Authorization",
                        token ?: throw TokenNotInitializedException()
                    ).build()
                )
                if (response.code == 401) {
                    // If the token was set but still unauthorized, token might be stale
                    // Therefore try again once with a new token
                    token =
                        GoogleAuthService(context).silentLogin()?.idToken
                    response = it.proceed(
                        it.request().newBuilder().addHeader(
                            "Authorization",
                            token ?: throw TokenNotInitializedException()
                        ).build()
                    )
                }
                response
            }

        if (Configuration.RETROFIT_LOGGING) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpClientBuilder.addInterceptor(loggingInterceptor)
        }

        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .registerTypeAdapter(OffsetDateTime::class.java, OffsetDateTimeDeserializer()).create()

        return Retrofit.Builder().baseUrl(Configuration.API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(okHttpClientBuilder.build())
            .build()
    }
}

// Custom Deserializer for the OffsetDateTime class as GSON wont handle it by default
class OffsetDateTimeDeserializer : JsonDeserializer<OffsetDateTime> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): OffsetDateTime {
        try {
            if (typeOfT === OffsetDateTime::class.java) {
                if (json != null) {
                    return OffsetDateTime.parse(
                        json.asJsonPrimitive.asString,
                        DateTimeFormatter.ISO_OFFSET_DATE_TIME
                    )
                }
            }
        } catch (e: DateTimeParseException) {
            throw JsonParseException(e)
        }
        throw IllegalArgumentException("unknown type: $typeOfT")
    }

}