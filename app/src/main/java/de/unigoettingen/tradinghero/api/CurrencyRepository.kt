package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import javax.inject.Inject

interface CurrencyInterface {
    @GET("/currencies")
    fun getListOfCurrencies(): Observable<Set<String>>
}

@Module
@InstallIn(ViewModelComponent::class)
class CurrencyRepository @Inject constructor(retrofit: Retrofit) {

    private val service: CurrencyInterface =
        retrofit.create(CurrencyInterface::class.java)

    fun getListOfCurrencies(): Observable<Set<String>> {
        return service.getListOfCurrencies()
    }
}
