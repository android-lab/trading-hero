package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.unigoettingen.tradinghero.model.FriendRequest
import de.unigoettingen.tradinghero.model.FriendShip
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import javax.inject.Inject

interface FriendRepositoryInterface {
    @GET("/friendship/")
    fun getFriends(): Observable<List<FriendShip>>

    @GET("/friendship/invites")
    fun getInvites(): Observable<List<FriendRequest>>

    @GET("/friendship/requests")
    fun getRequests(): Observable<List<FriendRequest>>

    @DELETE("/friendship/{friendId}")
    fun removeFriend(@Path("friendId") friendId: String): Observable<Void>

    @POST("/friendship/{friendId}")
    fun addFriend(@Path("friendId") friendId: String): Observable<FriendRequest?>
}

@Module
@InstallIn(ViewModelComponent::class)
class FriendRepository @Inject constructor(retrofit: Retrofit) {
    private val service: FriendRepositoryInterface =
        retrofit.create(FriendRepositoryInterface::class.java)

    fun getFriends() = service.getFriends()

    fun getInvites() = service.getInvites()

    fun getRequests() = service.getRequests()

    fun removeFriend(friendId: String) = service.removeFriend(friendId)

    fun addFriend(friendId: String) = service.addFriend(friendId)
}
