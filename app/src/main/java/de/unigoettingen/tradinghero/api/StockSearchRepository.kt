package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.unigoettingen.tradinghero.model.TinyStock
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject

interface StockSearchInterface {
    @GET("/stocks/search/{filter}")
    fun searchStock(
        @Path("filter") filter: String,
        @Query("price") price: Boolean
    ): Observable<List<TinyStock>>
}

@Module
@InstallIn(ViewModelComponent::class)
class StockSearchRepository @Inject constructor(retrofit: Retrofit) {

    private val service: StockSearchInterface =
        retrofit.create(StockSearchInterface::class.java)

    fun searchStock(filter: String, includePrice: Boolean): Observable<List<TinyStock>> {
        return service.searchStock(filter, includePrice)
    }
}
