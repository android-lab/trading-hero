package de.unigoettingen.tradinghero.api

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import de.unigoettingen.tradinghero.Configuration

// This service implements all the connections to sign in with google
// silentLogin will only succeed if there was already someone logged in at some point
class GoogleAuthService(private val context: Context) {
    private val googleSignInClient: GoogleSignInClient

    fun getIntent() = googleSignInClient.signInIntent

    init {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail()
            .requestIdToken(Configuration.CLIENT_ID)
            .build()
        googleSignInClient = GoogleSignIn.getClient(context, gso)
    }

    fun logout(onLogout: () -> Unit) {
        googleSignInClient.signOut().addOnCompleteListener {
            API.token = null
            onLogout()
        }
    }

    fun disconnect(onDisconnect: () -> Unit) {
        googleSignInClient.revokeAccess().addOnCompleteListener { onDisconnect() }
    }

    fun silentLogin(): GoogleSignInAccount? {
        val account: GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(context)
        if (account != null) {
            val task = googleSignInClient.silentSignIn()
            Tasks.await(task)
            return task.result
        }
        return null
    }

    fun silentLoginTask(): Task<GoogleSignInAccount>? {
        val account: GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(context)
        if (account != null) {
            return googleSignInClient.silentSignIn()
        }
        return null
    }

    fun handleSignInResult(
        completedTask: Task<GoogleSignInAccount>,
        onLogin: (GoogleSignInAccount?) -> Unit
    ) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            API.token = account?.idToken
            // Signed in successfully.
            onLogin(account)
        } catch (e: ApiException) {
            onLogin(null)
        }
    }
}
