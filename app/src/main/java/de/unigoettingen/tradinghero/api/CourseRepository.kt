package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.unigoettingen.tradinghero.model.Course
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject

interface CourseInterface {
    @GET("/stocks/pricing/{symbol}")
    fun getCourses(
        @Path("symbol") symbol: String,
        @Query("greaterDiffFromNow") greaterDiffFromNow: Long?,
        @Query("lesserDiffFromNow") lesserDiffFromNow: Long?,
        @Query("limit") limit: Int?,
        @Query("currency") currency: String?
    ): Observable<List<Course>>

    @GET("/stocks/pricing/current/{symbol}")
    fun getCourse(
        @Path("symbol") symbol: String,
        @Query("currency") currency: String?
    ): Observable<Course?>
}

@Module
@InstallIn(ViewModelComponent::class)
class CourseRepository @Inject constructor(retrofit: Retrofit) {

    private val service: CourseInterface =
        retrofit.create(CourseInterface::class.java)

    fun getCourse(
        symbol: String,
        greaterDiffFromNow: Long? = null,
        lesserDiffFromNow: Long? = null,
        limit: Int? = null,
        currency: String?
    ): Observable<List<Course>> {
        return service.getCourses(
            symbol,
            greaterDiffFromNow,
            lesserDiffFromNow,
            limit,
            currency
        )
    }

    fun getCurrentPrice(symbol: String, currency: String?): Observable<Course?> {
        return service.getCourse(symbol, currency)
    }
}
