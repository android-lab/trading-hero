package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.unigoettingen.tradinghero.model.Balance
import de.unigoettingen.tradinghero.model.Depot
import de.unigoettingen.tradinghero.model.DepotEntry
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject

interface DepotServiceInterface {
    @GET("/depot")
    fun getDepot(): Observable<Depot>

    @GET("/depot/{symbol}")
    fun getDepotEntry(@Path("symbol") symbol: String): Observable<DepotEntry>

    @GET("/balance")
    fun getBalance(@Query("currency") currency: String?): Observable<Balance>

    @GET("/depot/balance/{personId}")
    fun getDepotBalance(
        @Path("personId") personId: String,
        @Query("currency") currency: String?
    ): Observable<Balance>
}

@Module
@InstallIn(ViewModelComponent::class)
class DepotRepository @Inject constructor(retrofit: Retrofit) {

    private val service: DepotServiceInterface =
        retrofit.create(DepotServiceInterface::class.java)

    fun getDepot(): Observable<Depot> {
        return service.getDepot()
    }

    fun getDepotEntry(symbol: String) = service.getDepotEntry(symbol)

    fun getBalance(currency: String?) = service.getBalance(currency)

    fun getDepotBalance(personId: String, currency: String?) =
        service.getDepotBalance(personId, currency)
}
