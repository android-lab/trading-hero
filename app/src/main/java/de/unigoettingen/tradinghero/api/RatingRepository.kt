package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.unigoettingen.tradinghero.model.Rating
import de.unigoettingen.tradinghero.model.RatingType
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject

interface RatingServiceInterface {
    @GET("/rating/top/{type}")
    fun getTop(
        @Path("type") type: RatingType,
        @Query("amount") Amount: Int?
    ): Observable<List<Rating>>

    @GET("/rating/{type}/{symbol}")
    fun getPosition(
        @Path("type") type: RatingType,
        @Path("symbol") symbol: String
    ): Observable<Rating>
}

@Module
@InstallIn(ViewModelComponent::class)
class RatingRepository @Inject constructor(retrofit: Retrofit) {
    private val service: RatingServiceInterface =
        retrofit.create(RatingServiceInterface::class.java)

    fun getTop(type: RatingType, amount: Int?): Observable<List<Rating>> =
        service.getTop(type, amount)

    fun getPosition(type: RatingType, symbol: String): Observable<Rating> =
        service.getPosition(type, symbol)
}
