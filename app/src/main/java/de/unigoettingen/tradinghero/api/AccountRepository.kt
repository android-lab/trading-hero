package de.unigoettingen.tradinghero.api

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.unigoettingen.tradinghero.model.Person
import de.unigoettingen.tradinghero.model.PersonWriteView
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path
import javax.inject.Inject

interface AccountServiceInterface {
    @GET("/persons/me")
    fun getPerson(): Observable<Person>

    @GET("/persons/{personId}")
    fun getPerson(@Path("personId") personId: String): Observable<Person>

    @PUT("/persons/me")
    fun updatePerson(@Body personWriteView: PersonWriteView): Observable<Person>
}

@Module
@InstallIn(ViewModelComponent::class)
class AccountRepository @Inject constructor(retrofit: Retrofit) {

    private val service: AccountServiceInterface =
        retrofit.create(AccountServiceInterface::class.java)

    fun getPerson(): Observable<Person> {
        return service.getPerson()
    }

    fun getPerson(id: String): Observable<Person> {
        return service.getPerson(id)
    }

    fun updatePerson(person: PersonWriteView): Observable<Person> {
        return service.updatePerson(person)
    }
}
