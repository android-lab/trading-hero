package de.unigoettingen.tradinghero.exception

class RouteException(message: String? = null) : Throwable(message)
