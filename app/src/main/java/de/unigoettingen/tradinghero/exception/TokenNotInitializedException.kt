package de.unigoettingen.tradinghero.exception

class TokenNotInitializedException(message: String? = null) : Throwable(message)
