package de.unigoettingen.tradinghero.exception

class StockSymbolNotSetException(message: String? = null) : Throwable(message)
