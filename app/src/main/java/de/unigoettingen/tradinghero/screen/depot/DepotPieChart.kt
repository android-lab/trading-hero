package de.unigoettingen.tradinghero.screen.depot

import android.graphics.Paint
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ExperimentalGraphicsApi
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.unigoettingen.tradinghero.model.DepotListEntry
import de.unigoettingen.tradinghero.model.PieChartEntry
import de.unigoettingen.tradinghero.util.detectGestures
import de.unigoettingen.tradinghero.util.stringToColor
import kotlin.math.atan2

@ExperimentalGraphicsApi
@Composable
fun DepotPieChart(
    modifier: Modifier = Modifier,
    depotListEntries: List<DepotListEntry>,
    text: String,
    dividerLength: Float = 1F,
    animateFloat: Animatable<Float, AnimationVector1D>,
    onTouch: (String?) -> Unit
) {
    val totalAmount = depotListEntries.sumOf { it.amount * it.price }
    val selectedCourse = remember { mutableStateOf<String?>(null) }
    val angleList = mutableListOf<PieChartEntry>()
    val stroke = with(LocalDensity.current) { Stroke(10.dp.toPx()) }
    val strokeSelected = with(LocalDensity.current) { Stroke(20.dp.toPx()) }

    var amount = 0.0

    // Calculates the color, width and starting angle for each slice
    depotListEntries.forEach {
        val entryPrice = it.amount * it.price
        val startAngle =
            ((amount / totalAmount * 360).toFloat() + dividerLength / 2) * animateFloat.value + (90 * animateFloat.value)
        val sweepAngle =
            ((entryPrice / totalAmount * 360).toFloat() - dividerLength) * animateFloat.value
        angleList.add(PieChartEntry(it.name, startAngle, sweepAngle, stringToColor(it.name)))
        amount += entryPrice
    }

    // Create state from list to update component when angle list changes
    val stateList = remember {
        mutableStateOf(listOf<PieChartEntry>())
    }
    stateList.value = angleList

    val textColor = MaterialTheme.colors.primary.toArgb()
    // Canvas to draw the chart
    Canvas(
        modifier = modifier
            .fillMaxWidth()
            .aspectRatio(1F)
            .pointerInput(Unit) {
                detectGestures(
                    onDrag = { change ->
                        // Calculate angle of touch position from center
                        change.consumeAllChanges()
                        val x = change.position.x - size.width / 2
                        val y = change.position.y - size.height / 2

                        val angle = (Math.toDegrees(atan2(y, x).toDouble()) - 90).mod(360.0)

                        // Find course which is at the current angle
                        val course =
                            stateList.value.find {
                                (it.startAngle - 90) < angle && (it.startAngle - 90 + it.sweepAngle) > angle
                            }?.name

                        // Update selected course
                        onTouch(course)
                        selectedCourse.value = course
                    },
                    onDragEnd = {
                        // End of touch event
                        onTouch(null)
                        selectedCourse.value = null
                    }
                )
            },
        onDraw = {
            drawIntoCanvas { canvas ->
                // Draw all slices
                depotListEntries.forEachIndexed { i, depotEntry ->
                    drawArc(
                        color = angleList[i].color,
                        startAngle = angleList[i].startAngle,
                        sweepAngle = angleList[i].sweepAngle,
                        useCenter = false,
                        style = if (selectedCourse.value == depotEntry.name) strokeSelected else stroke
                    )
                    // Draw text in the middle of the chart
                    canvas.nativeCanvas.drawText(
                        text,
                        center.x,
                        center.y + 12.sp.toPx(),
                        Paint().apply {
                            isAntiAlias = true
                            textAlign = Paint.Align.CENTER
                            textSize = 24.sp.toPx()
                            color = textColor
                        }
                    )
                }
            }
        }
    )
}
