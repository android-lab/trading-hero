package de.unigoettingen.tradinghero.screen.setting

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Checkbox
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AttachMoney
import androidx.compose.material.icons.rounded.DarkMode
import androidx.compose.material.icons.rounded.Pin
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.alorma.settings.composables.SettingsCheckbox
import com.alorma.settings.composables.SettingsMenuLink
import de.unigoettingen.tradinghero.model.FormatterType
import de.unigoettingen.tradinghero.model.SettingRoutes
import de.unigoettingen.tradinghero.routing.Routes
import de.unigoettingen.tradinghero.ui.BackTopBar
import de.unigoettingen.tradinghero.ui.DialogWithCheckBoxList
import de.unigoettingen.tradinghero.util.Formatter
import de.unigoettingen.tradinghero.viewmodel.CurrencyViewModel
import de.unigoettingen.tradinghero.viewmodel.SettingsViewModel

@Composable
fun SettingsScreen(
    navController: NavController,
    settingRoute: SettingRoutes?,
) {
    // Use frame with back arrow to get to last screen and display the selected settings screen
    BackTopBar(navController, "Settings") {
        when (settingRoute) {
            null -> MainSettingsList(navController)
            SettingRoutes.CurrencySettings -> CurrencyList()
        }
    }
}

@Composable
fun MainSettingsList(
    navController: NavController,
    settingsViewModel: SettingsViewModel = hiltViewModel(),
) {
    val isFormatDialogOpen = remember { mutableStateOf(false) }
    val isDarkMode by settingsViewModel.isDarkModeEnabled.collectAsState(false)
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val format by settingsViewModel.format.collectAsState(initial = FormatterType.AMERICAN)

    Column(modifier = Modifier.padding(4.dp)) {
        // Checkbox to change dark mode
        SettingsCheckbox(
            icon = { Icon(imageVector = Icons.Rounded.DarkMode, contentDescription = null) },
            title = { Text(text = "Dark Mode") },
            checked = isDarkMode,
            onCheckedChange = { settingsViewModel.setDarkMode(it) }
        )
        Spacer(modifier = Modifier.height(4.dp))
        // Setting dialog to change format
        SettingsMenuLink(
            icon = {
                Icon(
                    imageVector = Icons.Rounded.Pin,
                    contentDescription = null
                )
            },
            title = { Text(text = "Currency Format") },
            subtitle = { Text(text = format.name) }
        ) {
            isFormatDialogOpen.value = true
        }
        Spacer(modifier = Modifier.height(4.dp))
        // Forward to setting screen where you can select the currency
        SettingsMenuLink(
            icon = {
                Icon(
                    imageVector = Icons.Rounded.AttachMoney,
                    contentDescription = null
                )
            },
            title = { Text(text = "Currency") },
            subtitle = { Text(text = currency) }
        ) {
            navController.navigate(Routes.settings(SettingRoutes.CurrencySettings))
        }
    }
    // Dialog to display formats
    DialogWithCheckBoxList(
        isOpen = isFormatDialogOpen.value,
        title = "Currency Format",
        values = FormatterType.values().map { it.name },
        selectedValue = format.name
    ) {
        isFormatDialogOpen.value = false
        it?.let { newFormatName ->
            settingsViewModel.setFormat(
                FormatterType.valueOf(
                    newFormatName
                )
            )
        }
    }
}

@Composable
fun CurrencyList(
    currencyViewModel: CurrencyViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel()
) {
    val currencies by currencyViewModel.currencyView.collectAsState()
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    // Load list of all currencies
    LaunchedEffect(Unit) {
        currencyViewModel.loadCurrencies()
    }
    // Display all currencies with a checkbox to change selected currency
    LazyColumn(modifier = Modifier.padding(4.dp)) {
        items(currencies.toList()) { text ->
            SettingsCheckbox(
                modifier = Modifier.height(50.dp),
                title = { Text(text = text) },
                checked = text == currency,
                onCheckedChange = {
                    Formatter.setCurrency(text)
                    settingsViewModel.setCurrency(text)
                }
            )
            Spacer(modifier = Modifier.height(4.dp))
        }
    }
}

@Composable
fun SimpleSettingsCheckbox(
    modifier: Modifier = Modifier,
    title: String,
    isChecked: Boolean,
    onCheckedChange: (Boolean) -> Unit,
) {
    Surface {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .clickable(onClick = { onCheckedChange(!isChecked) }),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = title)
            Checkbox(checked = isChecked, onCheckedChange = { onCheckedChange(!isChecked) })
        }
    }
}

// Load settings on app startup
@Composable
fun InitializeSettings(settingsViewModel: SettingsViewModel = hiltViewModel()) {
    val format by settingsViewModel.format.collectAsState(FormatterType.AMERICAN)
    val currency by settingsViewModel.currency.collectAsState("USD")

    // Initialize Formatter
    Formatter.setSymbols(format)
    Formatter.setCurrency(currency)
}
