package de.unigoettingen.tradinghero.screen.login

import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import de.unigoettingen.tradinghero.R
import de.unigoettingen.tradinghero.api.API
import de.unigoettingen.tradinghero.api.GoogleAuthService
import de.unigoettingen.tradinghero.routing.Routes

@Composable
fun LoginScreen(
    navController: NavController,
    routeTo: String? = null
) {
    val standardRoute = Routes.depot()
    val context = LocalContext.current
    val googleAuthService = GoogleAuthService(context)
    // Create launcher to start google sign in process
    val signInLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val task: Task<GoogleSignInAccount> =
                GoogleSignIn.getSignedInAccountFromIntent(result.data)
            googleAuthService.handleSignInResult(task) {
                if (it != null) {
                    navigateToRoute(navController, routeTo ?: standardRoute)
                }
            }
        }
    var isSilentLoginLoading by remember { mutableStateOf(true) }
    // Try to silent login
    LaunchedEffect(Unit) {
        googleAuthService.silentLoginTask()?.let { task ->
            task.addOnSuccessListener {
                API.token = it.idToken
                navigateToRoute(navController, routeTo ?: standardRoute)
            }
            task.addOnFailureListener { isSilentLoginLoading = false }
        } ?: run { isSilentLoginLoading = false }
    }
    // Only show login screen when silent login failed
    if (!isSilentLoginLoading) {
        Scaffold {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                // Logo
                Image(
                    painter = painterResource(R.drawable.d67d2c4ffab94d44b3fc3f1e7029c163),
                    contentDescription = null,
                    modifier = Modifier.size(250.dp)
                )
                Spacer(modifier = Modifier.height(140.dp))
                Text(
                    text = "Sign in or create an account",
                    fontSize = 17.sp,
                    modifier = Modifier.padding(16.dp)
                )
                // Google sign in button
                Button(
                    shape = RoundedCornerShape(10),
                    onClick = {
                        // Start google sign in
                        signInLauncher.launch(googleAuthService.getIntent())
                    },
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.White)
                ) {
                    Row {
                        Image(
                            painter = painterResource(R.drawable.googleg_standard_color_18),
                            contentDescription = "Login",
                            modifier = Modifier
                                .padding(start = 8.dp)
                                .size(18.dp)
                        )
                        Text(
                            text = "Google",
                            color = Color(0x8A000000),
                            fontFamily = FontFamily(Font(R.font.roboto_medium)),
                            fontSize = 14.sp,
                            modifier = Modifier.padding(start = 24.dp, end = 8.dp),
                            maxLines = 1
                        )
                    }
                }
            }
        }
    }
}

fun navigateToRoute(navController: NavController, route: String) {
    navController.navigate(route) {
        popUpTo(Routes.login()) { inclusive = true }
    }
}
