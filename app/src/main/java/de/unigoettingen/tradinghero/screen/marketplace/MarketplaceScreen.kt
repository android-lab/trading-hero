package de.unigoettingen.tradinghero.screen.marketplace

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import de.unigoettingen.tradinghero.model.RatingListEntry
import de.unigoettingen.tradinghero.model.RatingType
import de.unigoettingen.tradinghero.model.toTinyStock
import de.unigoettingen.tradinghero.routing.Routes
import de.unigoettingen.tradinghero.screen.search.SearchListItem
import de.unigoettingen.tradinghero.screen.search.SearchTopBar
import de.unigoettingen.tradinghero.ui.BackTopBar
import de.unigoettingen.tradinghero.ui.HorizontalListWithTitle
import de.unigoettingen.tradinghero.util.customPlaceholder
import de.unigoettingen.tradinghero.viewmodel.MarketplaceViewModel
import de.unigoettingen.tradinghero.viewmodel.SettingsViewModel

@Composable
fun MarketplaceScreen(navController: NavController, type: RatingType?) {
    // Show marketplace overview or a specialised view for only one rating
    when (type) {
        RatingType.SOCIAL -> BackTopBar(navController) {
            SocialRatingList(navController)
        }
        RatingType.PRICE -> BackTopBar(navController) {
            PriceRatingList(navController)
        }
        RatingType.MARKET_CAP -> BackTopBar(navController) {
            MarketCapRatingList(navController)
        }
        null -> SearchTopBar(navController) {
            Marketplace(navController)
        }
    }
}

@Composable
fun Marketplace(
    navController: NavController,
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    marketplaceViewModel: MarketplaceViewModel = hiltViewModel()
) {
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")

    val socialRatings by marketplaceViewModel.socialRatings.collectAsState()
    val priceRatings by marketplaceViewModel.priceRatings.collectAsState()
    val marketCapRatings by marketplaceViewModel.marketCapRatings.collectAsState()

    // Load all ratings
    LaunchedEffect(Unit) {
        marketplaceViewModel.loadAllRatings(currency)
    }

    // Shows a list for the top 20 social rated items
    Column(
        modifier = Modifier.background(
            brush = Brush.verticalGradient(
                colors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.background
                )
            )
        )
    ) {
        HorizontalListWithTitle(
            modifier = Modifier
                .height(128.dp)
                .fillMaxWidth(),
            title = marketplaceViewModel.socialTitle,
            onInvestigate = {
                navController.navigate(Routes.marketplace(RatingType.SOCIAL))
            }
        ) {
            if (socialRatings.isEmpty()) {
                items(5) {
                    MarketPlaceListItem(
                        entry = null
                    )
                }
            } else {
                items(items = socialRatings) {
                    MarketPlaceListItem(entry = it) {
                        navController.navigate(Routes.stock(it.stock.symbol))
                    }
                }
            }
        }
        // Shows a list for the top 20 stocks with the highest price
        HorizontalListWithTitle(
            modifier = Modifier
                .height(128.dp)
                .fillMaxWidth(),
            title = marketplaceViewModel.priceTitle,
            onInvestigate = {
                navController.navigate(Routes.marketplace(RatingType.PRICE))
            }
        ) {
            if (priceRatings.isEmpty()) {
                items(5) {
                    MarketPlaceListItem(
                        entry = null
                    )
                }
            } else {
                items(items = priceRatings) {
                    MarketPlaceListItem(entry = it) {
                        navController.navigate(Routes.stock(it.stock.symbol))
                    }
                }
            }
        }
        // Shows a list for the top 20 stocks with the highest market cap
        HorizontalListWithTitle(
            modifier = Modifier
                .height(128.dp)
                .fillMaxWidth(),
            title = marketplaceViewModel.marketCapTitle,
            onInvestigate = {
                navController.navigate(Routes.marketplace(RatingType.MARKET_CAP))
            }
        ) {
            if (marketCapRatings.isEmpty()) {
                items(5) {
                    MarketPlaceListItem(
                        entry = null
                    )
                }
            } else {
                items(items = marketCapRatings) {
                    MarketPlaceListItem(entry = it) {
                        navController.navigate(Routes.stock(it.stock.symbol))
                    }
                }
            }
        }
    }
}

// Displays a item use in the horizontal lists with a big logo and the stock symbol
@Composable
fun MarketPlaceListItem(
    modifier: Modifier = Modifier,
    entry: RatingListEntry?,
    onClick: () -> Unit = {}
) {
    Card(
        shape = RoundedCornerShape(15),
        modifier = modifier
            .fillMaxHeight()
            .width(128.dp)
            .padding(5.dp)
            .clip(RoundedCornerShape(15))
            .customPlaceholder(
                entry == null,
                MaterialTheme.colors.surface,
                RoundedCornerShape(15)
            )
            .clickable { onClick() },
    ) {
        Image(
            painter = rememberImagePainter(entry?.stock?.logo),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .background(Color.White)
                .clip(RoundedCornerShape(15)),
        )
        Column(
            modifier = Modifier
                .padding(5.dp)
                .fillMaxHeight()
        ) {
            Column(modifier = Modifier.fillMaxHeight(), verticalArrangement = Arrangement.Bottom) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(
                            MaterialTheme.colors.background.copy(alpha = 0.3F),
                            RoundedCornerShape(50)
                        ),
                    text = entry?.stock?.displaySymbol ?: "",
                    textAlign = TextAlign.Center,
                )
            }
        }
    }
}

// Shows a list view with more entries then the vertical list on the overview screen
@Composable
fun RatingList(navController: NavController, title: String, entries: List<RatingListEntry>) {
    Column {
        Text(modifier = Modifier.fillMaxWidth(), text = title, textAlign = TextAlign.Center)
        LazyColumn(modifier = Modifier.padding(horizontal = 16.dp)) {
            items(
                items = entries,
                itemContent = { item ->
                    SearchListItem(item.stock.toTinyStock()) {
                        navController.navigate(Routes.stock(item.stock.symbol))
                    }
                }
            )
        }
    }
}

// Rating list with the top 100 social rated stocks
@Composable
fun SocialRatingList(
    navController: NavController,
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    marketplaceViewModel: MarketplaceViewModel = hiltViewModel()
) {
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val socialRatings by marketplaceViewModel.socialRatings.collectAsState()
    LaunchedEffect(Unit) {
        marketplaceViewModel.loadRatings(currency, RatingType.SOCIAL, 100)
    }
    RatingList(navController, marketplaceViewModel.socialTitle, socialRatings)
}

// Rating list with the top 100 stocks with the highest price
@Composable
fun PriceRatingList(
    navController: NavController,
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    marketplaceViewModel: MarketplaceViewModel = hiltViewModel()
) {
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val priceRatings by marketplaceViewModel.priceRatings.collectAsState()
    LaunchedEffect(Unit) {
        marketplaceViewModel.loadRatings(currency, RatingType.PRICE, 100)
    }
    RatingList(navController, marketplaceViewModel.priceTitle, priceRatings)
}

// Rating list with the top 100 stocks with the market cap
@Composable
fun MarketCapRatingList(
    navController: NavController,
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    marketplaceViewModel: MarketplaceViewModel = hiltViewModel()
) {
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val marketCapRatings by marketplaceViewModel.marketCapRatings.collectAsState()
    LaunchedEffect(Unit) {
        marketplaceViewModel.loadRatings(currency, RatingType.MARKET_CAP, 100)
    }
    RatingList(navController, marketplaceViewModel.marketCapTitle, marketCapRatings)
}
