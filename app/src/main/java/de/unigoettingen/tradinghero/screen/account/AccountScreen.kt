package de.unigoettingen.tradinghero.screen.account

import android.text.format.DateFormat
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.sharp.Settings
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import de.unigoettingen.tradinghero.api.GoogleAuthService
import de.unigoettingen.tradinghero.model.PersonWriteView
import de.unigoettingen.tradinghero.routing.Routes
import de.unigoettingen.tradinghero.ui.BackTopBar
import de.unigoettingen.tradinghero.ui.LoadingScreen
import de.unigoettingen.tradinghero.viewmodel.PersonViewModel

@Composable
fun AccountScreen(navController: NavController) {
    val context = LocalContext.current
    BackTopBar(navController, actions = {
        IconButton(onClick = {
            navController.navigate(Routes.settings())
        }) {
            Icon(imageVector = Icons.Sharp.Settings, contentDescription = "Settings")
        }
    }) {
        Column(
            modifier = Modifier.background(
                brush = Brush.verticalGradient(
                    colors = listOf(
                        MaterialTheme.colors.surface,
                        MaterialTheme.colors.background
                    )
                )
            )
        ) {
            AccountOverview(modifier = Modifier.weight(1F))
            // Logout Button
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1F)
                    .padding(bottom = 64.dp),
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Button(
                    onClick = {
                        GoogleAuthService(context).logout {
                            navController.navigate(
                                Routes.login()
                            ) {
                                popUpTo(Routes.accountOverview()) { inclusive = true }
                            }
                        }
                    }
                ) {
                    Text(text = "Logout")
                }
            }
        }
    }
}

@Composable
fun AccountOverview(
    modifier: Modifier = Modifier,
    model: PersonViewModel = hiltViewModel()
) {
    val person by model.person.collectAsState()
    LaunchedEffect(Unit) {
        model.loadPerson()
    }
    if (person == null) {
        LoadingScreen()
        return
    }
    val animateNameWidth = remember { Animatable(0.5f) }
    LaunchedEffect(Unit) {
        animateNameWidth.animateTo(
            targetValue = 1f,
            animationSpec = tween(
                durationMillis = 300,
                easing = FastOutSlowInEasing
            )
        )
    }
    val animateEmailWidth = remember { Animatable(0.5f) }
    LaunchedEffect(Unit) {
        animateEmailWidth.animateTo(
            targetValue = 1f,
            animationSpec = tween(
                durationMillis = 350,
                easing = FastOutSlowInEasing
            )
        )
    }
    val animateMemberWidth = remember { Animatable(0.5f) }
    LaunchedEffect(Unit) {
        animateMemberWidth.animateTo(
            targetValue = 1f,
            animationSpec = tween(
                durationMillis = 400,
                easing = FastOutSlowInEasing
            )
        )
    }
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(16.dp),
    ) {
        Image(
            modifier = Modifier
                .padding(vertical = 16.dp)
                .size(80.dp)
                .clip(RoundedCornerShape(20)),
            painter = rememberImagePainter(person?.image),
            contentDescription = null,
        )
        ToggleableTextField(
            Modifier.fillMaxWidth(animateNameWidth.value),
            defaultValue = person?.name ?: "",
            isEditable = true,
            label = "Name",
        ) { name ->
            if (!name.isNullOrBlank()) {
                person?.let {
                    model.updatePerson(
                        PersonWriteView(
                            name,
                            it.image,
                            person?.familyName ?: name,
                            person?.givenName ?: name,
                        ),
                        shouldRetryOnTimeout = false,
                    )
                }
            }
        }
        ToggleableTextField(
            Modifier.fillMaxWidth(animateEmailWidth.value),
            defaultValue = person?.email ?: "",
            label = "Email"
        )
        ToggleableTextField(
            Modifier.fillMaxWidth(animateMemberWidth.value),
            defaultValue = DateFormat.format("dd.MM.yyyy", person?.dateCreated).toString(),
            label = "Member since"
        )
    }
}

// Text field that can be toggled to be editable
// Used so the styling stays the same in the AccountOverview for editable and uneditable text fields
@Composable
fun ToggleableTextField(
    modifier: Modifier = Modifier,
    defaultValue: String,
    isEditable: Boolean = false,
    label: String? = null,
    onFinishEditing: (newValue: String?) -> Unit = {},
) {
    val isEditing = remember { mutableStateOf(false) }
    val focusRequester = remember { FocusRequester() }
    val focusController = LocalFocusManager.current
    val textFieldValue = remember {
        mutableStateOf(
            TextFieldValue(defaultValue, TextRange(defaultValue.length))
        )
    }

    LaunchedEffect(defaultValue) {
        textFieldValue.value = TextFieldValue(defaultValue, TextRange(defaultValue.length))
    }
    LaunchedEffect(isEditing.value) {
        if (isEditing.value) {
            focusRequester.requestFocus()
        }
    }

    OutlinedTextField(
        // As disabled fields can not be focused this solves some issues with the custom focusing.
        // Unfortunately makes the fields not copyable.
        enabled = isEditing.value,
        // Style the disabled text field the same as the enabled text field
        colors = TextFieldDefaults.outlinedTextFieldColors(
            disabledBorderColor = MaterialTheme.colors.onSurface.copy(
                alpha = ContentAlpha.disabled
            ),
            disabledTextColor = LocalContentColor.current.copy(LocalContentAlpha.current),
            disabledLabelColor = MaterialTheme.colors.onSurface.copy(ContentAlpha.medium)
        ),
        modifier = modifier.focusRequester(focusRequester),
        readOnly = !isEditing.value,
        singleLine = true,
        value = textFieldValue.value,
        onValueChange = { textFieldValue.value = it },
        trailingIcon = {
            if (isEditable) {
                if (isEditing.value) {
                    Row {
                        IconButton(onClick = {
                            isEditing.value = false
                            focusController.clearFocus()
                            onFinishEditing(textFieldValue.value.text)
                        }, content = {
                            Icon(imageVector = Icons.Filled.Done, contentDescription = "Save")
                        })
                        IconButton(onClick = {
                            isEditing.value = false
                            focusController.clearFocus()
                            // Reset text field value back to original string
                            textFieldValue.value =
                                TextFieldValue(defaultValue, TextRange(defaultValue.length))
                            onFinishEditing(null)
                        }, content = {
                            Icon(imageVector = Icons.Filled.Close, contentDescription = "Cancel")
                        })
                    }
                } else {
                    IconButton(onClick = {
                        isEditing.value = true
                    }, content = {
                        Icon(imageVector = Icons.Filled.Edit, contentDescription = "Edit")
                    })
                }
            }
        },
        shape = if (isEditing.value) {
            MaterialTheme.shapes.large
        } else {
            MaterialTheme.shapes.medium
        },
        label = {
            if (!label.isNullOrBlank()) {
                Text(label)
            }
        }
    )
}
