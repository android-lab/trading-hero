package de.unigoettingen.tradinghero.screen.friend

import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.rounded.Add
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import de.unigoettingen.tradinghero.Configuration
import de.unigoettingen.tradinghero.api.API
import de.unigoettingen.tradinghero.model.FriendEntry
import de.unigoettingen.tradinghero.routing.Routes
import de.unigoettingen.tradinghero.screen.search.SearchTopBar
import de.unigoettingen.tradinghero.util.Formatter.formatCurrency
import de.unigoettingen.tradinghero.util.customPlaceholder
import de.unigoettingen.tradinghero.viewmodel.FriendListViewModel
import de.unigoettingen.tradinghero.viewmodel.PersonViewModel
import de.unigoettingen.tradinghero.viewmodel.SettingsViewModel

@Composable
fun FriendScreen(
    navController: NavController,
    friendId: String?,
    friendListViewModel: FriendListViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel()
) {

    if (API.token == null) {
        // If we are not logged in yet
        navController.navigate(Routes.login(Routes.friends(friendId)))
    }
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val friends = friendListViewModel.friends.collectAsState()
    val invites = friendListViewModel.invites.collectAsState()
    val requests = friendListViewModel.requests.collectAsState()
    val swipeState = rememberSwipeRefreshState(false)

    // Load all sent or received friend requests and all friends
    LaunchedEffect(Unit) {
        friendListViewModel.loadInvites()
        friendListViewModel.loadFriends(currency)
        friendListViewModel.loadRequests()
    }

    // Show friend request dialog when the router had a friendId
    friendId?.let {
        FriendRequestDialog(friendId)
    }
    // Use search bar as an outer frame
    SearchTopBar(navController) {
        // Swipe to refresh your friends
        SwipeRefresh(state = swipeState, onRefresh = {
            swipeState.isRefreshing = true
            var friendsLoaded = false
            var requestsLoaded = false
            var invitesLoaded = false
            friendListViewModel.loadInvites {
                invitesLoaded = true
                if (friendsLoaded && requestsLoaded && invitesLoaded) {
                    swipeState.isRefreshing = false
                }
            }
            friendListViewModel.loadFriends(currency) {
                friendsLoaded = true
                if (friendsLoaded && requestsLoaded && invitesLoaded) {
                    swipeState.isRefreshing = false
                }
            }
            friendListViewModel.loadRequests {
                requestsLoaded = true
                if (friendsLoaded && requestsLoaded && invitesLoaded) {
                    swipeState.isRefreshing = false
                }
            }
        }) {
            // Show all friend requests, friend invites and friends
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .background(
                        brush = Brush.verticalGradient(
                            colors = listOf(
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.background
                            )
                        )
                    )
            ) {
                item {
                    FriendInvitations(invites.value)
                }
                item {
                    FriendList(friends.value)
                }
                item {
                    FriendRequests(requests.value)
                }
            }
        }
    }
}

// Shows a list of all friend requests you sent
@Composable
fun FriendRequests(
    requests: List<FriendEntry>?
) {
    if (requests?.isNotEmpty() == true) {
        Text(
            text = "Requests Sent",
            modifier = Modifier
                .padding(
                    start = 32.dp,
                    end = 32.dp,
                    top = 16.dp,
                    bottom = 16.dp
                ),
            fontSize = 30.sp
        )

        requests
            .sortedBy { it.name.uppercase() }
            .groupBy { it.name[0].uppercaseChar() }
            .forEach { (_, items) ->
                Row(
                    modifier = Modifier.padding(start = 10.dp),
                ) {
                    GroupHeader(items[0])
                }
                items.forEach {
                    Row(
                        modifier = Modifier.padding(start = 10.dp, bottom = 16.dp)
                    ) {
                        Spacer(modifier = Modifier.width(10.dp))
                        FriendRequestItem(friendEntry = it)
                    }
                }
            }
    }
}

// Shows a list of all your friends and their current balance. On long click you can remove them
@Composable
fun FriendList(
    friends: List<FriendEntry>?
) {
    Row(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = "Your Friends",
            modifier = Modifier
                .padding(16.dp, bottom = 0.dp),
            fontSize = 30.sp
        )
        ShareButton()
    }

    friends?.let { unmodifiedFriendEntries ->
        if (unmodifiedFriendEntries.isEmpty()) {
            Text(
                text = "No friends found",
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                fontSize = 14.sp
            )
        } else {
            unmodifiedFriendEntries
                .sortedBy { it.name.uppercase() }
                .groupBy { it.name[0].uppercaseChar() }
                .forEach { (_, items) ->
                    Row(
                        modifier = Modifier.padding(start = 10.dp),
                    ) {
                        GroupHeader(items[0])
                    }
                    items.forEach {
                        Row(
                            modifier = Modifier.padding(start = 10.dp, bottom = 16.dp)
                        ) {
                            Spacer(modifier = Modifier.width(10.dp))
                            FriendListItem(friendEntry = it)
                        }
                    }
                }
        }
    } ?: Spacer(
        modifier = Modifier
            .fillMaxWidth()
            .height(500.dp)
            .customPlaceholder(true, color = MaterialTheme.colors.surface)
    )
}

// Shows a list of all friend requests you received and you can accept or deny them
@Composable
fun FriendInvitations(
    invitations: List<FriendEntry>?
) {
    if (invitations?.isNotEmpty() == true) {
        Text(
            text = "Requests Received",
            modifier = Modifier
                .padding(
                    start = 32.dp,
                    end = 32.dp,
                    top = 16.dp,
                    bottom = 16.dp
                ),
            fontSize = 30.sp
        )

        invitations
            .sortedBy { it.name.uppercase() }
            .groupBy { it.name[0].uppercaseChar() }
            .forEach { (_, items) ->
                Row(
                    modifier = Modifier.padding(start = 10.dp),
                ) {
                    GroupHeader(items[0])
                }
                items.forEach {
                    Row(
                        modifier = Modifier.padding(start = 10.dp, bottom = 16.dp)
                    ) {
                        Spacer(modifier = Modifier.width(10.dp))
                        FriendInvitationItem(friendEntry = it)
                    }
                }
            }
    }
}

// Shows a header for each starting char for the friend lists
@Composable
fun GroupHeader(
    friendEntry: FriendEntry
) {
    val startingChar = friendEntry.name[0]
    val text = if (startingChar.isHighSurrogate()) {
        "\uD83D\uDCB8"
    } else {
        startingChar.toString()
    }
    Text(
        text = text,
        modifier = Modifier
            .alpha(0.7F),
        fontSize = 12.sp
    )
}

// The share button allows you to share your friend link to get new friends
@Composable
fun ShareButton(personViewModel: PersonViewModel = hiltViewModel()) {
    val person = personViewModel.person.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(Unit) {
        personViewModel.loadPerson()
    }

    val shareIntent = Intent().apply {
        action = Intent.ACTION_SEND
        type = "text/plain"

        putExtra(
            Intent.EXTRA_TEXT,
            "Add me on Trading Hero as friend:\n" +
                String.format(Configuration.FRIEND_INVITATION_URI, person.value?.id)
        )
    }
    IconButton(onClick = {
        context.startActivity(
            Intent.createChooser(
                shareIntent,
                "Share your friend request"
            )
        )
    }) {
        Icon(
            imageVector = Icons.Rounded.Add,
            contentDescription = null
        )
    }
}

// Item to show a friend and his current balance
@Composable
fun FriendListItem(
    modifier: Modifier = Modifier,
    friendEntry: FriendEntry,
    onClick: () -> Unit = {}
) {
    val isDeleteDialogOpen = remember { mutableStateOf(false) }

    DeleteFriendDialog(isOpen = isDeleteDialogOpen.value, friendEntry = friendEntry) {
        isDeleteDialogOpen.value = false
    }

    Column(
        modifier
            .fillMaxWidth()
            .pointerInput(Unit) {
                detectTapGestures(
                    onPress = { onClick() },
                    onLongPress = { isDeleteDialogOpen.value = true }
                )
            }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = rememberImagePainter(friendEntry.imageUrl),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(horizontal = 12.dp)
                        .size(32.dp)
                        .clip(RoundedCornerShape(50))
                )
                Column {
                    Text(
                        text = friendEntry.name
                    )
                    Text(
                        text = formatCurrency(friendEntry.balance),
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 1,
                        modifier = Modifier.padding(end = 12.dp),
                        fontSize = 13.sp
                    )
                }
            }
        }
    }
}

// Is visible when you long click a FriendItem to remove him from your friend list
@Composable
fun DeleteFriendDialog(
    isOpen: Boolean,
    friendEntry: FriendEntry,
    friendListViewModel: FriendListViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    onComplete: () -> Unit
) {
    val context = LocalContext.current
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")

    if (!isOpen) {
        return
    }

    AlertDialog(
        onDismissRequest = { onComplete() },
        title = {
            Text(text = friendEntry.name)
        },
        text = {
            Text(text = "Do you want to remove ${friendEntry.name} as friend?")
        },
        buttons = {
            Row(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.End
            ) {
                Button(
                    onClick = { onComplete() },
                    modifier = Modifier.padding(end = 4.dp)
                ) {
                    Text(text = "Cancel")
                }

                Button(onClick = {
                    friendListViewModel.removeFriend(friendEntry.id) {
                        friendListViewModel.loadFriends(currency)
                    }

                    Toast.makeText(
                        context,
                        "Friend ${friendEntry.name} was removed",
                        Toast.LENGTH_SHORT
                    ).show()
                    onComplete()
                }) {
                    Text(text = "Remove friendship")
                }
            }
        }
    )
}

// Item to display a friend you sent a request to
@Composable
fun FriendRequestItem(
    modifier: Modifier = Modifier,
    friendEntry: FriendEntry,
    onClick: () -> Unit = {}
) {
    Column(
        modifier
            .fillMaxWidth()
            .clickable { onClick() }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = rememberImagePainter(friendEntry.imageUrl),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(horizontal = 12.dp)
                        .size(32.dp)
                        .clip(RoundedCornerShape(50))
                )
                Text(text = friendEntry.name)
            }
        }
    }
}

// Item to display a friend who sent you a request and you can accept or deny it
@Composable
fun FriendInvitationItem(
    modifier: Modifier = Modifier,
    friendEntry: FriendEntry,
    friendListViewModel: FriendListViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    onClick: () -> Unit = {}
) {
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    Row(
        modifier
            .fillMaxWidth()
            .clickable { onClick() },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Row(
            modifier = Modifier
                .weight(0.9F),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = rememberImagePainter(friendEntry.imageUrl),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(horizontal = 12.dp)
                        .size(32.dp)
                        .clip(RoundedCornerShape(50))
                )
                Text(text = friendEntry.name)
            }
        }
        Row {
            IconButton(
                modifier = Modifier.padding(end = 5.dp),
                onClick = {
                    friendListViewModel.addFriend(friendEntry.id) {
                        friendListViewModel.loadFriends(currency)
                    }
                }
            ) {
                Icon(Icons.Filled.Done, null)
            }
            IconButton(
                modifier = Modifier.padding(start = 5.dp),
                onClick = {
                    friendListViewModel.removeFriend(friendEntry.id) {
                        friendListViewModel.loadInvites()
                    }
                }
            ) {
                Icon(Icons.Filled.Clear, null)
            }
        }
    }
}

@Composable
fun FriendRequestDialog(
    friendId: String,
    friendListViewModel: FriendListViewModel = hiltViewModel(),
    personViewModel: PersonViewModel = hiltViewModel()
) {
    val person = personViewModel.person.collectAsState()
    val friends = friendListViewModel.friends.collectAsState()
    val requests = friendListViewModel.requests.collectAsState()
    val friend = friendListViewModel.friend.collectAsState()

    val isDialogOpen = remember { mutableStateOf(true) }

    val context = LocalContext.current

    LaunchedEffect(Unit) {
        friendListViewModel.getFriend(friendId)
    }

    if (!isDialogOpen.value) {
        return
    }

    AlertDialog(
        modifier = Modifier.customPlaceholder(
            friends.value == null || requests.value == null || friend.value == null,
            color = MaterialTheme.colors.surface,
        ),
        onDismissRequest = {
            isDialogOpen.value = false
        },
        title = {
            Text(text = "Friend Request")
        },
        text = {
            friend.value?.name?.let { friendName ->
                when {
                    friends.value?.any { it.id == friendId } == true -> {
                        Text(text = "You are already friends with $friendName")
                    }
                    requests.value?.any { it.id == friendId } == true -> {
                        Text(text = "You have already send a request to $friendName")
                    }
                    friendId == person.value?.id -> {
                        Text(text = "You cannot add yourself as friend")
                    }
                    friendName.isNotEmpty() -> {
                        Text(text = "Send a friendship request to $friendName")
                    }
                    else -> {
                        Text(text = "User not found")
                    }
                }
            }
        },
        buttons = {
            Row(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.End
            ) {
                Button(
                    onClick = {
                        isDialogOpen.value = false
                    },
                    modifier = Modifier.padding(end = 4.dp)
                ) {
                    Text(text = "Cancel")
                }
                friendId.let {
                    if (friends.value?.none { friend -> friend.id == it } == true &&
                        requests.value?.none { request -> request.id == it } == true &&
                        it != person.value?.id
                    ) {
                        Button(onClick = {
                            friendListViewModel.addFriend(it) {
                                friendListViewModel.loadRequests()
                            }
                            Toast.makeText(context, "Request sent", Toast.LENGTH_SHORT)
                                .show()
                            isDialogOpen.value = false
                        }) {
                            Text(text = "Send")
                        }
                    }
                }
            }
        }
    )
}
