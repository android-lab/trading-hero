package de.unigoettingen.tradinghero.screen.stock

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Canvas
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.PaintingStyle
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.unigoettingen.tradinghero.model.Course
import de.unigoettingen.tradinghero.model.Resolution
import de.unigoettingen.tradinghero.stylesheet.negativeChangeColor
import de.unigoettingen.tradinghero.stylesheet.positiveChangeColor
import de.unigoettingen.tradinghero.util.Formatter.formatDecimal
import de.unigoettingen.tradinghero.util.Formatter.formatCurrency
import de.unigoettingen.tradinghero.util.detectGestures
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.absoluteValue

@Composable
fun LineChart(
    modifier: Modifier = Modifier,
    height: Float = 260F,
    spacing: Float = 30F,
    courseList: List<Course>,
    onResolutionChange: (Resolution) -> Unit = {}
) {
    val selectedCourse: MutableState<Course?> = remember {
        mutableStateOf(null)
    }

    Column(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val firstPrice = courseList.firstOrNull()?.price ?: 0.0
        val lastPrice = courseList.lastOrNull()?.price ?: 0.0
        val change = (selectedCourse.value?.price ?: 1.0) / firstPrice * 100 - 100

        PricingText(
            modifier,
            selectedCourse.value,
            firstPrice,
            lastPrice,
            change
        )
        if (courseList.isEmpty()) {
            LineChartGraph(
                modifier = modifier,
                chartHeight = height,
                spacing = spacing,
                courseList = listOf()
            )
        } else {
            LineChartGraph(
                modifier = modifier,
                chartHeight = height,
                spacing = spacing,
                courseList = courseList,
                onDrag = {
                    selectedCourse.value = it
                }
            )
        }

        ResolutionButtons(onResolutionChange)
    }
}

@Composable
fun PricingText(
    modifier: Modifier = Modifier,
    selectedCourse: Course?,
    firstPrice: Double,
    lastPrice: Double,
    change: Double
) {
    val text =
        selectedCourse?.let {
            "${formatCurrency(firstPrice)} ► " +
                    formatCurrency(it.price)
        } ?: formatCurrency(lastPrice)

    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(bottom = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(0.9F)
                .height(45.dp)
        ) {
            Text(
                text = text,
                textAlign = TextAlign.End,
                modifier = Modifier.fillMaxWidth(),
                fontSize = 20.sp
            )

            selectedCourse?.let { course ->
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())
                            .format(Date(course.timeStamp)),
                        textAlign = TextAlign.End,
                        fontSize = 14.sp
                    )

                    Text(
                        text = "${if (change >= 0) "+" else ""} ${formatDecimal(change)}%",
                        textAlign = TextAlign.End,
                        fontSize = 14.sp,
                        color = if (change >= 0) positiveChangeColor else negativeChangeColor
                    )
                }
            }
        }
    }
}

@Composable
fun ResolutionButtons(onResolutionChange: (Resolution) -> Unit) {
    val resolution = remember {
        mutableStateOf(Resolution.WEEK)
    }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceAround,
        modifier = Modifier
            .padding(top = 20.dp)
            .padding(horizontal = 16.dp)
            .fillMaxWidth()
    ) {
        Resolution.values().forEach {
            val border = if (resolution.value == it) BorderStroke(2.dp, positiveChangeColor) else null
            OutlinedButton(
                onClick = {
                    resolution.value = it
                    onResolutionChange(it)
                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
                border = border
            ) {
                Text(
                    text = it.displayName,
                    fontSize = 12.sp,
                )
            }
        }
    }
}

@Composable
fun LineChartGraph(
    modifier: Modifier = Modifier,
    chartHeight: Float,
    spacing: Float,
    courseList: List<Course>,
    onDrag: (nearestCourse: Course?) -> Unit = {}
) {
    if (courseList.isEmpty()) {
        Column(
            modifier = modifier
                .fillMaxWidth()
                .height(chartHeight.dp)
        ) {}
        return
    }

    val xPositionOfFinger = remember { mutableStateOf(0F) }

    val nearestCourseToFinger: MutableState<Course?> = remember { mutableStateOf(null) }

    val positions = calculatePositions(chartHeight, spacing, courseList) ?: return
    val lineColor = MaterialTheme.colors.secondaryVariant
    Canvas(
        modifier = modifier
            .fillMaxWidth()
            .height(chartHeight.dp)
            .pointerInput(Unit) {
                detectGestures(
                    onDrag = { change ->
                        change.consumeAllChanges()
                        xPositionOfFinger.value = change.position.x

                        val nearestCourse =
                            positions.minByOrNull { (_, offset) ->
                                // Smallest distance to finger position
                                (offset.x * size.width - xPositionOfFinger.value).absoluteValue
                            }?.first

                        nearestCourseToFinger.value = nearestCourse
                        onDrag(nearestCourse)
                    },
                    onDragEnd = {
                        onDrag(null)
                        nearestCourseToFinger.value = null
                    }
                )
            },

        ) {
        drawIntoCanvas { canvas ->
            drawBottomLine(canvas, density, chartHeight, size, 1F, lineColor)
            drawChart(canvas, density, positions, lineColor, size)
            nearestCourseToFinger.value?.let {
                drawSelectionLine(canvas, density, xPositionOfFinger, chartHeight, lineColor)
                drawPoint(
                    canvas,
                    density,
                    positions,
                    it,
                    size,
                    xPositionOfFinger.value
                )
            }
        }
    }
}

fun drawPoint(
    canvas: Canvas,
    density: Float,
    positions: List<Pair<Course, Offset>>,
    nearestCourseToFinger: Course,
    size: Size,
    xPositionOfFinger: Float
) {
    val nearestCoursePosition = positions.find {
        it.first == nearestCourseToFinger
    } ?: return

    val furtherCoursePosition =
        getFurtherCoursePosition(
            positions,
            positions.indexOf(nearestCoursePosition),
            xPositionOfFinger,
            size.width
        )

    val nearestCourseOffset = nearestCoursePosition.second

    // Will never be true, but for "division by 0" safety
    if (furtherCoursePosition.x == nearestCourseOffset.x) {
        return
    }
    val heightDiff = furtherCoursePosition.y - nearestCourseOffset.y
    val widthDiffBetweenFingerAndCourse = xPositionOfFinger - nearestCourseOffset.x * size.width
    val widthDiffBetweenCourseAndFurtherCourse =
        (furtherCoursePosition.x - nearestCourseOffset.x) * size.width
    val addedHeight =
        heightDiff * widthDiffBetweenFingerAndCourse / widthDiffBetweenCourseAndFurtherCourse

    val pointPosition = Offset(
        xPositionOfFinger,
        (nearestCoursePosition.second.y + addedHeight).dp.value * density
    )
    canvas.drawCircle(
        center = pointPosition,
        radius = 4.dp.value * density,
        paint = Paint().apply {
            isAntiAlias = true
            color = positiveChangeColor
            style = PaintingStyle.Fill
            strokeWidth = 40.dp.value * density
        }
    )
}

fun drawSelectionLine(
    canvas: Canvas,
    density: Float,
    xPositionOfFinger: MutableState<Float>,
    chartHeight: Float,
    lineColor: Color
) {
    canvas.drawLine(
        p1 = Offset(xPositionOfFinger.value, 0F),
        p2 = Offset(xPositionOfFinger.value, chartHeight.dp.value * density),
        paint = Paint().apply {
            isAntiAlias = true
            color = lineColor
            style = PaintingStyle.Fill
            strokeWidth = 1.dp.value * density
        }
    )
}

fun drawBottomLine(
    canvas: Canvas,
    density: Float,
    chartHeight: Float,
    size: Size,
    strokeWidth: Float,
    lineColor: Color
) {
    canvas.drawLine(
        p1 = Offset(0F, chartHeight.dp.value * density),
        p2 = Offset(size.width, chartHeight.dp.value * density),
        paint = Paint().apply {
            this.isAntiAlias = true
            this.color = lineColor
            this.style = PaintingStyle.Stroke
            this.strokeWidth = strokeWidth.dp.value * density
        }
    )
}

fun drawChart(
    canvas: Canvas,
    density: Float,
    positions: List<Pair<Course, Offset>>,
    lineColor: Color,
    size: Size
) {
    positions.map { it.second }.zipWithNext { previousPosition, position ->
        canvas.drawLine(
            p1 = Offset(previousPosition.x * size.width, previousPosition.y.dp.value * density),
            p2 = Offset(position.x * size.width, position.y.dp.value * density),
            paint = Paint().apply {
                isAntiAlias = true
                color = lineColor
                style = PaintingStyle.Stroke
                strokeWidth = 2F
            }
        )
    }
}

fun calculatePositions(
    chartHeight: Float,
    spacing: Float,
    courses: List<Course>
): List<Pair<Course, Offset>>? {

    val coursesSortedByTime: List<Course> =
        courses.sortedBy { it.timeStamp }

    val minPrice = coursesSortedByTime.minByOrNull { it.price }?.price ?: return null
    val maxPrice = coursesSortedByTime.maxByOrNull { it.price }?.price ?: return null
    val totalPriceDiff = maxPrice - minPrice

    val minTime = coursesSortedByTime.firstOrNull()?.timeStamp ?: return null
    val maxTime = coursesSortedByTime.lastOrNull()?.timeStamp ?: return null
    val averageTimeBetweenCourses =
        calculateAverageTimeBetweenCourses(coursesSortedByTime)

    // As all the time, where the stock market is closed, is not to be shown on the chart,
    // it has to be taken into account.
    val totalSkippableTime = getTotalSkippableTime(coursesSortedByTime, averageTimeBetweenCourses)
    // Time distance without weekends
    val totalTimeDiff = maxTime - minTime - totalSkippableTime

    var skippedTime = 0.0
    return coursesSortedByTime.mapIndexed { index, course ->
        val timeDiffToPreviousCourse =
            if (index != 0) course.timeStamp - coursesSortedByTime[index - 1].timeStamp else 0L
        // The stock market was closed between these course updates,
        // indicated by the time difference being larger than usual.
        // Therefore we want to ignore this value and use the average time between courses instead
        if (timeDiffToPreviousCourse > averageTimeBetweenCourses * 2) {
            skippedTime += timeDiffToPreviousCourse - averageTimeBetweenCourses
        }
        val x = ((course.timeStamp - skippedTime - minTime) / totalTimeDiff).toFloat()

        val relativePriceDiff = (course.price - minPrice) / totalPriceDiff
        // Spacing calculation to get relative chart height
        val y = chartHeight - (relativePriceDiff * (chartHeight - spacing) + spacing).toFloat()

        Pair(course, Offset(x, y))
    }
}

private fun getTotalSkippableTime(
    sortedCourseList: List<Course>,
    averageTimeDistance: Double
) = sortedCourseList.zipWithNext { previousCourse, course -> // time of all weekends
    val diff = course.timeStamp - previousCourse.timeStamp

    if (diff < averageTimeDistance * 2) {
        0.0
    } else {
        diff - averageTimeDistance
    }
}.sum()

// Get the position of the Course
// which is the further one
// (of the two Courses that the point is in between)
// from the dragging point.
// With x,y,z being the Course Positions and o being the position of the dragging point
// z-----x-o---y
// This function will return the position of y
// z---o-x-----y
// This function will return the position of z
// z-o----x-----y
// This function will return the position of x
fun getFurtherCoursePosition(
    coursePositions: List<Pair<Course, Offset>>,
    nearestCourseIndex: Int,
    currentDragXPosition: Float,
    chartWidth: Float
): Offset {
    if (nearestCourseIndex == 0) {
        return coursePositions[nearestCourseIndex + 1].second
    }

    if (nearestCourseIndex + 1 >= coursePositions.size) {
        return coursePositions[nearestCourseIndex - 1].second
    }

    val selectedCourseOffset = coursePositions[nearestCourseIndex].second
    val beforeCourseOffset = coursePositions[nearestCourseIndex - 1].second
    val nextCourseOffset = coursePositions[nearestCourseIndex + 1].second

    return if (currentDragXPosition > selectedCourseOffset.x * chartWidth) {
        nextCourseOffset
    } else {
        beforeCourseOffset
    }
}

private fun calculateAverageTimeBetweenCourses(courses: List<Course>): Double {
    if (courses.size <= 1) {
        return 0.0
    }
    val cumulativeTimeBetweenAllCourses = courses
        .zipWithNext { previousCourse, course ->
            (course.timeStamp - previousCourse.timeStamp).absoluteValue.toDouble()
        }
        .sum()
    return cumulativeTimeBetweenAllCourses / (courses.size - 1)
}
