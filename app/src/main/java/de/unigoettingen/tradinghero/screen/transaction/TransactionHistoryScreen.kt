package de.unigoettingen.tradinghero.screen.transaction

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import de.unigoettingen.tradinghero.model.Transaction
import de.unigoettingen.tradinghero.model.TransactionType
import de.unigoettingen.tradinghero.stylesheet.negativeChangeColor
import de.unigoettingen.tradinghero.stylesheet.positiveChangeColor
import de.unigoettingen.tradinghero.stylesheet.splitColor
import de.unigoettingen.tradinghero.ui.BackTopBar
import de.unigoettingen.tradinghero.util.Formatter.formatCurrency
import de.unigoettingen.tradinghero.util.Formatter.formatDate
import de.unigoettingen.tradinghero.util.Formatter.setCurrency
import de.unigoettingen.tradinghero.viewmodel.TransactionViewModel
import java.time.OffsetDateTime
import java.util.*

@Composable
fun TransactionHistoryScreen(
    navController: NavController,
    symbol: String,
    transactionViewModel: TransactionViewModel = hiltViewModel()
) {
    val transactions = transactionViewModel.transactions.collectAsState()

    LaunchedEffect(Unit) {
        transactionViewModel.loadTransactionHistory(symbol)
    }
    BackTopBar(navController, "$symbol Transaction History") {
        LazyColumn {
            item { Spacer(modifier = Modifier.height(16.dp)) }
            transactions.value.reversed().forEach {
                item { TransactionListItem(transaction = it) }
            }
        }
    }
}

@Composable
fun TransactionListItem(transaction: Transaction) {
    val borderColor = when (transaction.type) {
        TransactionType.BUY -> positiveChangeColor
        TransactionType.SELL -> negativeChangeColor
        TransactionType.SPLIT -> splitColor
    }
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp)
            .padding(bottom = 16.dp)
            .fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .border(width = 2.dp, color = borderColor, shape = MaterialTheme.shapes.medium)
                .padding(16.dp)
        ) {
            Text(
                modifier = Modifier.weight(1F),
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                text = "${transaction.type}",
            )
            Text(
                modifier = Modifier.weight(2F),
                textAlign = TextAlign.Center,
                text = "${transaction.amount.toInt()} @ ${formatCurrency(transaction.price)}",
                // maxLines = 1
            )
            Text(
                modifier = Modifier.weight(1F),
                textAlign = TextAlign.Center,
                text = formatDate(Date(transaction.date.toEpochSecond() * 1000)),
            )
        }
    }
}

@Preview
@Composable
fun BuyPreview() {
    setCurrency("USD")
    TransactionListItem(Transaction("TEST", 2.0, TransactionType.BUY, 20.0, OffsetDateTime.now()))
}

@Preview
@Composable
fun SellPreview() {
    setCurrency("USD")
    TransactionListItem(Transaction("TEST", 12.0, TransactionType.SELL, 20.0, OffsetDateTime.now()))
}

@Preview
@Composable
fun SplitPreview() {
    setCurrency("USD")
    TransactionListItem(
        Transaction(
            "TEST",
            222.0,
            TransactionType.SPLIT,
            20.0,
            OffsetDateTime.now()
        )
    )
}

@Preview
@Composable
fun AmountPreview() {
    setCurrency("USD")
    TransactionListItem(
        Transaction(
            "TEST",
            2999999999999999.0,
            TransactionType.BUY,
            20.0,
            OffsetDateTime.now()
        )
    )
}

@Preview
@Composable
fun PricePreview() {
    setCurrency("USD")
    TransactionListItem(
        Transaction(
            "TEST",
            2.0,
            TransactionType.BUY,
            2099999999999.0,
            OffsetDateTime.now()
        )
    )
}
