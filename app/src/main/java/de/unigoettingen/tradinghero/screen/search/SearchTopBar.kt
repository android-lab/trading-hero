package de.unigoettingen.tradinghero.screen.search

import androidx.activity.compose.BackHandler
import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.rounded.Person
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import de.unigoettingen.tradinghero.model.TinyStock
import de.unigoettingen.tradinghero.routing.Routes
import de.unigoettingen.tradinghero.ui.StandardTopBar
import de.unigoettingen.tradinghero.ui.BottomNavigationBar
import de.unigoettingen.tradinghero.viewmodel.PersonViewModel
import de.unigoettingen.tradinghero.viewmodel.SearchResultViewModel

@Composable
fun SearchTopBar(
    navController: NavController,
    searchResultViewModel: SearchResultViewModel = hiltViewModel(),
    personViewModel: PersonViewModel = hiltViewModel(),
    content: @Composable () -> Unit
) {
    val search by searchResultViewModel.search.collectAsState()
    val person by personViewModel.person.collectAsState()

    LaunchedEffect(Unit) {
        personViewModel.loadPerson()
    }

    val filter = remember {
        mutableStateOf("")
    }
    val isSearchFieldHoldingText = remember {
        mutableStateOf(false)
    }
    val isSearchFieldExtended = remember { mutableStateOf(false) }
    val focusRequester = remember { FocusRequester() }

    // Custom handler for controlling behaviour of back button when search bar is open
    BackHandler(isSearchFieldExtended.value) {
        isSearchFieldExtended.value = false
        isSearchFieldHoldingText.value = false
        filter.value = ""
    }
    Scaffold(
        topBar = {
            StandardTopBar(
                actions = {
                    // Show search bar in actions from top bar
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        SearchBar(
                            filter,
                            isSearchFieldExtended,
                            onValueChanged = {

                                isSearchFieldHoldingText.value = it.isNotBlank()
                                filter.value = it
                                if (filter.value.isNotBlank()) {
                                    searchResultViewModel.searchStock(filter.value, false)
                                }
                            },
                            onCancelClicked = {
                                isSearchFieldExtended.value = false
                                isSearchFieldHoldingText.value = false
                                filter.value = ""
                            },
                            focusRequester
                        )
                        // Show logo to navigate to account overview
                        IconButton(
                            modifier = Modifier
                                .size(52.dp)
                                .padding(end = 10.dp),
                            onClick = { navController.navigate(Routes.accountOverview()) }
                        ) {
                            if (person == null) {
                                Icon(
                                    modifier = Modifier
                                        .size(28.dp),
                                    imageVector = Icons.Rounded.Person,
                                    contentDescription = "profile"
                                )
                            } else {
                                Image(
                                    modifier = Modifier
                                        .clip(RoundedCornerShape(50))
                                        .size(28.dp),
                                    painter = rememberImagePainter(person?.image),
                                    contentDescription = "profile"
                                )
                            }
                        }
                    }
                }
            )
        },
        bottomBar = { BottomNavigationBar(navController = navController) }
    ) {
        Box(modifier = Modifier.padding(it)) {

            // Crossfade between content  which can be passed from outside and search view
            Crossfade(
                targetState = isSearchFieldHoldingText.value,
                animationSpec = tween(200)
            ) { searchView ->
                when (searchView) {
                    true -> StockList(navController, search)
                    false -> content()
                }
            }
        }
    }
}

// Shows a list of all stocks which can be clicked to forward you to the stock screen
@Composable
fun StockList(
    navController: NavController,
    list: List<TinyStock>
) {
    LazyColumn(
        Modifier
            .padding(horizontal = 16.dp)
    ) {
        item {
            Spacer(modifier = Modifier.height(16.dp))
        }
        items(
            items = list,
            itemContent = { item ->
                SearchListItem(item) {
                    navController.navigate(Routes.stock(item.symbol))
                }
            }
        )
        item {
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

// Displays a stock icon, symbol and name
@Composable
fun SearchListItem(item: TinyStock, onClick: () -> Unit = {}) {
    Card(
        shape = RoundedCornerShape(25),
        modifier = Modifier
            .padding(bottom = 4.dp)
            .fillMaxWidth()
            .clip(RoundedCornerShape(25))
            .clickable(onClick = onClick)
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Image(
                modifier = Modifier
                    .padding(start = 12.dp)
                    .clip(RoundedCornerShape(50))
                    .size(32.dp),
                painter = rememberImagePainter(item.logo),
                contentDescription = null
            )
            Column(Modifier.padding(14.dp)) {
                Text(
                    text = item.name,
                )
                Text(
                    text = item.symbol,
                    fontSize = 12.sp,
                )
            }
        }
    }
}

// Search bar which extends when clicked on. Text sets the filter to search for stocks and display them in the search list
@Composable
fun SearchBar(
    filter: MutableState<String> = mutableStateOf(""),
    isExtended: MutableState<Boolean> = mutableStateOf(false),
    onValueChanged: (String) -> Unit = {},
    onCancelClicked: () -> Unit = {},
    focusRequester: FocusRequester
) {
    val animateSearchBar = remember { Animatable(0f) }
    // Animate open and close effect
    LaunchedEffect(isExtended.value) {
        if (!isExtended.value) {
            animateSearchBar.animateTo(
                targetValue = 0f,
                animationSpec = tween(
                    durationMillis = 500,
                    easing = FastOutSlowInEasing
                )
            )
        } else {
            animateSearchBar.animateTo(
                targetValue = 1f,
                animationSpec = tween(
                    durationMillis = 500,
                    easing = FastOutSlowInEasing
                )
            )
        }
    }
    Crossfade(
        targetState = isExtended.value,
        animationSpec = tween(200)
    ) { extended ->
        when (extended) {
            // Show search icon when not extended
            false -> IconButton(
                modifier = Modifier.size(52.dp),
                onClick = { isExtended.value = true }
            ) {
                Icon(
                    Icons.Default.Search,
                    contentDescription = "",
                    modifier = Modifier
                        .padding(15.dp)
                        .size(24.dp)
                )
            }

            // Show text field when extended
            true -> {
                TextField(
                    value = filter.value,
                    onValueChange = onValueChanged,
                    modifier = Modifier
                        .fillMaxWidth(animateSearchBar.value)
                        .focusRequester(focusRequester),
                    textStyle = TextStyle(color = Color.White, fontSize = 18.sp),
                    leadingIcon = {
                        Icon(
                            Icons.Default.Search,
                            contentDescription = "",
                            modifier = Modifier
                                .padding(15.dp)
                                .size(24.dp)
                        )
                    },
                    trailingIcon = {

                        IconButton(
                            onClick = onCancelClicked
                        ) {
                            Icon(
                                Icons.Default.Close,
                                contentDescription = "",
                                modifier = Modifier
                                    .padding(15.dp)
                                    .size(24.dp)
                            )
                        }
                    },
                    singleLine = true,
                    shape = RectangleShape,
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent
                    )
                )
                // Get focus when opened -> keyboard extends
                LaunchedEffect(Unit) {
                    focusRequester.requestFocus()
                }
            }
        }
    }
}
