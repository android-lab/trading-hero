package de.unigoettingen.tradinghero.screen.transaction

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Dialpad
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import de.unigoettingen.tradinghero.model.TransactionType
import de.unigoettingen.tradinghero.model.TransactionWrite
import de.unigoettingen.tradinghero.util.Formatter.formatCurrency
import de.unigoettingen.tradinghero.viewmodel.DepotViewModel
import de.unigoettingen.tradinghero.viewmodel.SettingsViewModel
import de.unigoettingen.tradinghero.viewmodel.StockDepotEntryViewModel
import de.unigoettingen.tradinghero.viewmodel.TransactionViewModel

@Composable
fun BuyDialog(
    isOpen: Boolean,
    symbol: String,
    stockDepotEntryViewModel: StockDepotEntryViewModel = hiltViewModel(),
    transactionViewModel: TransactionViewModel = hiltViewModel(),
    depotViewModel: DepotViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    onClose: () -> Unit
) {
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val depotEntry = stockDepotEntryViewModel.depotScreenEntry.collectAsState()
    val currentPrice = stockDepotEntryViewModel.currentPrice.collectAsState()
    val amount = remember { mutableStateOf<Int?>(null) }
    val balance = depotViewModel.balance.collectAsState()

    LaunchedEffect(Unit) {
        depotViewModel.loadBalance(currency)
    }

    val context = LocalContext.current

    val failedToast = Toast.makeText(context, "Failed to Buy", Toast.LENGTH_SHORT)
    val successfulToast =
        Toast.makeText(context, "Bought ${amount.value} $symbol", Toast.LENGTH_SHORT)

    TradeDialog(
        isOpen = isOpen,
        symbol = symbol,
        actionText = "BUY",
        priceText = currentPrice.value?.let { formatCurrency(it) },
        onValueChanged = {
            amount.value = it
        },
        isSaveButtonDisabledCondition = {
            val safeBalance = balance.value?.amount
            val safePrice = currentPrice.value
            it == null
                    || it < 0
                    || safePrice == null
                    || safeBalance == null
                    || safeBalance < safePrice * it.toInt()
        },
        purchaseSummary = {
            val safeBalance = balance.value?.amount
            val safePrice = currentPrice.value
            val safeDepotEntry = depotEntry.value
            if (safeBalance != null && safePrice != null && safeDepotEntry != null) {
                Column {
                    Spacer(modifier = Modifier.height(8.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(text = "Price: ")
                        Text(text = formatCurrency(safePrice * (amount.value ?: 0)))
                    }
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(text = "Remaining Balance: ")
                        Text(
                            text = formatCurrency(
                                safeBalance - (safePrice * (amount.value ?: 0))
                            )
                        )
                    }
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(text = "Stocks after Purchase: ")
                        Text(text = "${((amount.value ?: 0) + safeDepotEntry.amount).toInt()}")
                    }
                }
            }
        }
    ) { hasCanceled ->
        if (hasCanceled) {
            amount.value = null
            onClose()
        } else {
            amount.value?.let { value ->
                transactionViewModel.makeTransaction(
                    TransactionWrite(
                        symbol,
                        value.toDouble(),
                        TransactionType.BUY
                    ),
                    onError = {
                        failedToast.show()
                    },
                    onComplete = {
                        stockDepotEntryViewModel.loadDepotEntity(symbol)
                        transactionViewModel.loadTransactionHistory(symbol)
                        depotViewModel.loadBalance(currency)
                        successfulToast.show()
                    }
                )
                amount.value = null
                onClose()
            } ?: run { failedToast.show() }
        }
    }
}

@Composable
fun SellDialog(
    isOpen: Boolean,
    symbol: String,
    stockDepotEntryViewModel: StockDepotEntryViewModel = hiltViewModel(),
    transactionViewModel: TransactionViewModel = hiltViewModel(),
    depotViewModel: DepotViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    onClose: () -> Unit
) {
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val depotEntry = stockDepotEntryViewModel.depotScreenEntry.collectAsState()
    val currentPrice = stockDepotEntryViewModel.currentPrice.collectAsState()
    val balance = depotViewModel.balance.collectAsState()
    val amount = remember { mutableStateOf<Int?>(null) }
    LaunchedEffect(Unit) {
        depotViewModel.loadBalance(currency)
    }
    val context = LocalContext.current

    val failedToast = Toast.makeText(context, "Failed to Sell", Toast.LENGTH_SHORT)
    val successfulToast =
        Toast.makeText(context, "Sold ${amount.value} $symbol", Toast.LENGTH_SHORT)
    TradeDialog(
        isOpen = isOpen,
        symbol = symbol,
        actionText = "SELL",
        priceText = currentPrice.value?.let { formatCurrency(it) },
        onValueChanged = {
                amount.value = it
        },
        isSaveButtonDisabledCondition = {
            val safeDepotEntryAmount = depotEntry.value?.amount
            it == null
                    || safeDepotEntryAmount == null
                    || it > safeDepotEntryAmount
        },
        purchaseSummary = {
            val safeBalance = balance.value
            val safePrice = currentPrice.value
            val safeDepotEntry = depotEntry.value
            if (safeBalance != null && safePrice != null && safeDepotEntry != null) {
                Column {
                    Spacer(modifier = Modifier.height(8.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(text = "New Balance: ")
                        Text(
                            text = formatCurrency(
                                safeBalance.amount + (safePrice * (amount.value ?: 0))
                            )
                        )
                    }
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(text = "Stocks after Sell: ")
                        Text(text = "${(safeDepotEntry.amount - (amount.value ?: 0)).toInt()}")
                    }
                }
            }

        }
    ) { hasCanceled ->
        if (hasCanceled) {
            amount.value = null
            onClose()
        } else {
            amount.value?.let { value ->
                transactionViewModel.makeTransaction(
                    TransactionWrite(
                        symbol,
                        value.toDouble(),
                        TransactionType.SELL
                    ),
                    onError = {
                        failedToast.show()
                    },
                    onComplete = {
                        stockDepotEntryViewModel.loadDepotEntity(symbol)
                        depotViewModel.loadBalance(currency)
                        successfulToast.show()
                    }
                )
                amount.value = null
                onClose()
            } ?: run { failedToast.show() }
        }
    }
}

@Composable
fun TradeDialog(
    isOpen: Boolean,
    symbol: String,
    actionText: String,
    priceText: String?,
    purchaseSummary: @Composable () -> Unit,
    isSaveButtonDisabledCondition: (Int?) -> Boolean,
    onValueChanged: (Int?) -> Unit,
    onClick: (hasCanceled: Boolean) -> Unit
) {
    if (!isOpen) {
        return
    }

    val amount = remember { mutableStateOf("") }

    AlertDialog(
        onDismissRequest = { onClick(true) },
        title = {
            Column {
                Text(text = "$actionText $symbol")
                priceText?.let {
                    Text(
                        text = priceText, fontSize = 10.sp,
                        color = MaterialTheme.colors.secondaryVariant.copy(alpha = 0.6F)
                    )
                }
            }
        },
        text = {
            Column {
                TextField(
                    value = amount.value,
                    onValueChange = { value ->
                        amount.value = value
                        onValueChanged(amount.value.toIntOrNull())
                    },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    shape = RectangleShape,
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = Color.Transparent,
                    ),
                    trailingIcon = {
                        Icon(
                            imageVector = Icons.Sharp.Dialpad,
                            contentDescription = null
                        )
                    }
                )
                purchaseSummary()
            }
        },
        buttons = {
            Row(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.End
            ) {
                Button(
                    onClick = { onClick(true) },
                    modifier = Modifier.padding(end = 4.dp)
                ) {
                    Text(text = "Cancel")
                }
                Button(
                    enabled = !isSaveButtonDisabledCondition(amount.value.toIntOrNull()),
                    onClick = { onClick(false) }) {
                    Text(text = actionText)
                }
            }
        }
    )
}

@Preview
@Composable
fun TradeDialogPreview(){
    TradeDialog(
        actionText = "Purchase",
        isOpen = true,
        symbol = "SYMBL",
        priceText = "Preis",
        purchaseSummary = {Text("Summary")},
        isSaveButtonDisabledCondition = {false},
        onClick = {},
        onValueChanged = {}
    )
}