package de.unigoettingen.tradinghero.screen.stock

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import de.unigoettingen.tradinghero.model.DepotScreenEntry
import de.unigoettingen.tradinghero.model.RatingType
import de.unigoettingen.tradinghero.model.Resolution
import de.unigoettingen.tradinghero.model.Stock
import de.unigoettingen.tradinghero.routing.Routes
import de.unigoettingen.tradinghero.screen.depot.DepotItem
import de.unigoettingen.tradinghero.screen.search.SearchTopBar
import de.unigoettingen.tradinghero.screen.transaction.BuyDialog
import de.unigoettingen.tradinghero.screen.transaction.SellDialog
import de.unigoettingen.tradinghero.stylesheet.negativeChangeColor
import de.unigoettingen.tradinghero.stylesheet.positiveChangeColor
import de.unigoettingen.tradinghero.ui.InfoCategory
import de.unigoettingen.tradinghero.ui.ListEntry
import de.unigoettingen.tradinghero.ui.LoadingScreen
import de.unigoettingen.tradinghero.ui.InfoBoxWithCategorySwitcher
import de.unigoettingen.tradinghero.util.Formatter.formatCurrency
import de.unigoettingen.tradinghero.util.customPlaceholder
import de.unigoettingen.tradinghero.viewmodel.*

@Composable
fun StockScreen(
    navController: NavController,
    symbol: String,
    stockViewModel: StockViewModel = hiltViewModel(),
    courseViewModel: CourseViewModel = hiltViewModel(),
    stockDepotEntryViewModel: StockDepotEntryViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel()
) {
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val stock = stockViewModel.stock.collectAsState()

    // Load all stock information and its course for the last week
    LaunchedEffect(Unit) {
        stockViewModel.getStockInformation(symbol, currency)
        stockDepotEntryViewModel.loadDepotEntity(symbol)
        stockDepotEntryViewModel.loadCurrentPrice(symbol, currency)
        courseViewModel.getCourse(
            symbol = symbol,
            greaterDiffFromNow = Resolution.WEEK.duration,
            currency = currency
        )
    }

    // Show search bar and account logo
    SearchTopBar(navController) {
        stock.value?.let {
            StockDetailOverview(
                navController,
                it
            )
        } ?: LoadingScreen()
    }
}

@Composable
fun StockDetailOverview(
    navController: NavController,
    stock: Stock,
    courseViewModel: CourseViewModel = hiltViewModel(),
    stockDepotEntryViewModel: StockDepotEntryViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel(),
    marketplaceViewModel: MarketplaceViewModel = hiltViewModel()
) {
    val depotEntry by stockDepotEntryViewModel.depotScreenEntry.collectAsState()
    val currentPrice by stockDepotEntryViewModel.currentPrice.collectAsState()
    val currency by settingsViewModel.currency.collectAsState(initial = "USD")
    val courseList = courseViewModel.course.collectAsState()

    val socialRating by marketplaceViewModel.socialRating.collectAsState()
    val priceRating by marketplaceViewModel.priceRating.collectAsState()
    val marketCapRating by marketplaceViewModel.marketCapRating.collectAsState()

    // Load rating of stock
    LaunchedEffect(Unit) {
        marketplaceViewModel.loadRating(RatingType.MARKET_CAP, stock.symbol)
        marketplaceViewModel.loadRating(RatingType.SOCIAL, stock.symbol)
        marketplaceViewModel.loadRating(RatingType.PRICE, stock.symbol)
    }



    LazyColumn(
        modifier = Modifier.fillMaxWidth().background(
            brush = Brush.verticalGradient(
                colors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.background
                )
            )
        ),
        verticalArrangement = Arrangement.Center
    ) {
        item {
            // Display the company name and  if the stock is rated in the top 5 of a marketplace category show a badge accordingly
            Row(
                modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = stock.name,
                    modifier = Modifier
                        .weight(1F),
                    fontSize = 30.sp,
                    textAlign = TextAlign.Start
                )
                if (listOf(
                        socialRating,
                        priceRating,
                        marketCapRating
                    ).any { it != null && it.placement <= 5 }
                ) {
                    Column(
                        modifier = Modifier
                            .weight(1F),
                        horizontalAlignment = Alignment.End,
                        verticalArrangement = Arrangement.SpaceBetween
                    ) {
                        socialRating?.takeIf { it.placement <= 5 }?.let {
                            Text(
                                text = "#${it.placement} ${marketplaceViewModel.socialTitle}",
                                fontSize = 12.sp
                            )
                        }
                        priceRating?.takeIf { it.placement <= 5 }?.let {
                            Text(
                                text = "#${it.placement} ${marketplaceViewModel.priceTitle}",
                                fontSize = 12.sp
                            )
                        }
                        marketCapRating?.takeIf { it.placement <= 5 }?.let {
                            Text(
                                text = "#${it.placement} ${marketplaceViewModel.marketCapTitle}",
                                fontSize = 12.sp
                            )
                        }
                    }
                }
            }
        }
        item {
            // Show interactive line chart with course information
            LineChart(
                modifier = Modifier
                    .customPlaceholder(
                        isVisible = courseList.value.isEmpty(),
                        color = MaterialTheme.colors.surface,
                        shape = RectangleShape,
                    ),
                courseList = courseList.value,
                onResolutionChange = {
                    courseViewModel.resetCourseList()
                    courseViewModel.getCourse(
                        symbol = stock.symbol,
                        greaterDiffFromNow = it.duration,
                        currency = currency
                    )
                    stockDepotEntryViewModel.loadCurrentPrice(stock.symbol, currency)
                }
            )
        }
        item { Spacer(Modifier.height(16.dp)) }
        item {
            // Depot item with the current number of stocks you own and their value
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
            ) {
                DepotItem(
                    modifier = Modifier.customPlaceholder(
                        isVisible = depotEntry == null || currentPrice == null,
                        color = MaterialTheme.colors.surface,
                        shape = RoundedCornerShape(50)
                    ),
                    symbol = stock.symbol,
                    item = DepotScreenEntry(
                        stock = stock,
                        amount = depotEntry?.amount ?: 0.0,
                        currentPrice = currentPrice ?: 0.0
                    )
                )
            }
        }
        item { Spacer(Modifier.height(16.dp)) }
        item {
            // Trade buttons to buy or sell stocks
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                TradeButtons(
                    Modifier.customPlaceholder(
                        isVisible = depotEntry == null || currentPrice == null,
                        shape = RoundedCornerShape(50),
                        color = MaterialTheme.colors.surface
                    ),
                    stock
                )

            }
        }
        // Spacer is in THBtn included
        item {
            // Shows transaction history button if any transactions are available
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp)
            ) {
                TransactionHistoryButton(navController, stock.symbol)
            }
        }
        item { Spacer(Modifier.height(16.dp)) }
        item {
            StockInformation(stock)
        }
        item { Spacer(Modifier.height(32.dp)) }
    }
}

@Composable
fun StockInformation(stock: Stock) {
    val websiteIntent = remember { Intent(Intent.ACTION_VIEW, Uri.parse(stock.url)) }
    val context = LocalContext.current

    InfoBoxWithCategorySwitcher(
        categories = listOf(
            InfoCategory("General Information") {
                ListEntry(title = "Name", value = stock.name)
                Divider()
                ListEntry(title = "Symbol", value = stock.symbol)
                Divider()
                ListEntry(title = "Description", value = stock.description)
                Divider()
                ListEntry(title = "Industry", value = stock.industry)
                Divider()
                ListEntry(title = "Website", value = stock.url) {
                    context.startActivity(websiteIntent)
                }
            },
            InfoCategory("Stock Data") {
                ListEntry(title = "Stock Market", value = stock.exchange)
                Divider()
                ListEntry(title = "Stock Type", value = stock.type)
                Divider()
                ListEntry(title = "Figi", value = stock.figi)
                Divider()
                ListEntry(title = "Mic", value = stock.mic)
                Divider()
                ListEntry(
                    title = "Market Cap",
                    value = formatCurrency(stock.marketCapitalization, 6)
                )
            }
        )
    )
}

@Composable
fun TradeButtons(
    modifier: Modifier,
    stock: Stock
) {
    val isSellDialogOpen = remember { mutableStateOf(false) }
    val isBuyDialogOpen = remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {

            Button(
                onClick = { isBuyDialogOpen.value = true },
                modifier = modifier
                    .weight(1F)
                    .padding(end = 8.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = positiveChangeColor)
            ) {
                Text(text = "Buy", modifier = Modifier.padding(5.dp))
            }
            Button(
                onClick = { isSellDialogOpen.value = true },
                modifier = modifier.weight(1F),
                colors = ButtonDefaults.buttonColors(backgroundColor = negativeChangeColor)
            ) {
                Text(text = "Sell", modifier = Modifier.padding(5.dp))
            }
            BuyDialog(isBuyDialogOpen.value, stock.symbol){isBuyDialogOpen.value = false}
            SellDialog(isSellDialogOpen.value, stock.symbol){isSellDialogOpen.value = false}
        }
    }
}

@Composable
fun TransactionHistoryButton(
    navController: NavController,
    symbol: String,
    transactionViewModel: TransactionViewModel = hiltViewModel()
) {
    val transactions by transactionViewModel.transactions.collectAsState()

    LaunchedEffect(Unit) {
        transactionViewModel.loadTransactionHistory(symbol)
    }
    if (transactions.isEmpty()) {
        return
    }
    Spacer(Modifier.height(16.dp))
    Button(
        onClick = { navController.navigate(Routes.transactionHistory(symbol)) },
        modifier = Modifier.fillMaxWidth(),
        colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.surface)
    ) {
        Text(text = "Transaction History", modifier = Modifier.padding(5.dp))
    }
}
