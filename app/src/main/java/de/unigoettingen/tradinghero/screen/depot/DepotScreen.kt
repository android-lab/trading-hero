package de.unigoettingen.tradinghero.screen.depot

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Paid
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ExperimentalGraphicsApi
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import de.unigoettingen.tradinghero.model.DepotListEntry
import de.unigoettingen.tradinghero.model.DepotScreenEntry
import de.unigoettingen.tradinghero.routing.Routes
import de.unigoettingen.tradinghero.screen.search.SearchTopBar
import de.unigoettingen.tradinghero.ui.LoadingScreen
import de.unigoettingen.tradinghero.util.Formatter.formatCurrency
import de.unigoettingen.tradinghero.util.customPlaceholder
import de.unigoettingen.tradinghero.viewmodel.DepotViewModel
import de.unigoettingen.tradinghero.viewmodel.SettingsViewModel

@ExperimentalGraphicsApi
@Composable
fun DepotScreen(
    navController: NavController,
    depotViewModel: DepotViewModel = hiltViewModel(),
    settingsViewModel: SettingsViewModel = hiltViewModel()
) {
    SearchTopBar(navController = navController) {
        val depot by depotViewModel.depot.collectAsState()
        val balance by depotViewModel.balance.collectAsState()
        val currency by settingsViewModel.currency.collectAsState(initial = "USD")
        val animateFloat = remember { Animatable(0f) }
        var selectedDepotItem by remember {
            mutableStateOf<String?>(null)
        }

        // Load depot
        LaunchedEffect(Unit) {
            depotViewModel.loadDepot(currency)
            depotViewModel.loadBalance(currency)
        }
        if (depot.stocks == null) {
            LoadingScreen()
        }
        depot.stocks?.let { stocks ->
            // Animate pie chart
            LaunchedEffect(Unit) {
                animateFloat.animateTo(
                    targetValue = 1f,
                    animationSpec = tween(
                        durationMillis = 700,
                        easing = FastOutSlowInEasing
                    )
                )
            }
            LazyColumn(
                modifier = Modifier
                    .background(
                        brush = Brush.verticalGradient(
                            colors = listOf(
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.background
                            )
                        )
                    )
                    .padding(horizontal = 16.dp)
            ) {
                item {
                    Column {
                        // Creates a List with all depot entrys
                        val depotListEntry = stocks.map {
                            DepotListEntry(
                                it.key,
                                it.value.amount ?: 0.0,
                                it.value.currentPrice ?: 0.0
                            )
                        }.toMutableList()

                        // Adds balance to the list
                        balance?.let {
                            depotListEntry.add(DepotListEntry("Balance", 1.0, it.amount))
                        }
                        // Tests if all stocks are loaded
                        val loaded = !stocks.any {
                            listOfNotNull(
                                it.value.amount,
                                it.value.currentPrice,
                                it.value.stock
                            ).size < 3
                        }
                        // Show "Loading..." or the sum of all stock values
                        val pieChartText = if (loaded) {
                            formatCurrency(depotListEntry.sumOf { it.amount * it.price })
                        } else {
                            "Loading..."
                        }
                        // Show depot pie chart
                        DepotPieChart(
                            modifier = Modifier.padding(16.dp),
                            depotListEntries = depotListEntry.filter { it.amount != 0.0 }
                                .toMutableList()
                                .sortedBy { it.price * it.amount },
                            text = pieChartText,
                            animateFloat = animateFloat
                        ) {
                            selectedDepotItem = it
                        }
                    }
                }
                item {
                    // Show selected item at the top of all depot entries
                    selectedDepotItem?.let { symbol ->
                        stocks[symbol]?.let { depotScreenEntry ->
                            Spacer(Modifier.height(4.dp))
                            DepotItem(
                                modifier = Modifier.customPlaceholder(
                                    isVisible = listOfNotNull(
                                        depotScreenEntry.amount,
                                        depotScreenEntry.currentPrice,
                                        depotScreenEntry.stock
                                    ).size < 3,
                                    color = MaterialTheme.colors.surface,
                                    shape = RoundedCornerShape(50)
                                ),
                                symbol = symbol,
                                item = depotScreenEntry,
                                isSelected = true
                            ) {
                                navController.navigate(Routes.stock(it))
                            }
                        }
                    }
                }
                item { Spacer(Modifier.height(4.dp)) }
                item {
                    // Show balance item
                    BalanceItem(
                        modifier = Modifier.customPlaceholder(
                            isVisible = balance == null,
                            color = MaterialTheme.colors.surface,
                            shape = RoundedCornerShape(50)
                        ),
                        balance = balance?.amount,
                        isSelected = selectedDepotItem == "Balance"
                    )
                }
                item { Spacer(Modifier.height(4.dp)) }
                // Show all depot entries except the selected depot item
                items(
                    items = stocks.filter { it.value.amount != 0.0 && selectedDepotItem != it.key }
                        .toList()
                        .sortedByDescending {
                            (it.second.currentPrice ?: 1.0) * (it.second.amount ?: 1.0)
                        },
                    itemContent = { (symbol, entry) ->
                        DepotItem(
                            modifier = Modifier.customPlaceholder(
                                isVisible = listOfNotNull(
                                    entry.amount,
                                    entry.currentPrice,
                                    entry.stock
                                ).size < 3,
                                color = MaterialTheme.colors.surface,
                                shape = RoundedCornerShape(50)
                            ),
                            symbol = symbol,
                            item = entry
                        ) {
                            navController.navigate(Routes.stock(it))
                        }
                        Spacer(modifier = Modifier.height(4.dp))
                    }
                )
            }
        }
    }
}

// Displays a depot item with its icon, name, amount and current value
@Composable
fun DepotItem(
    modifier: Modifier = Modifier,
    symbol: String,
    item: DepotScreenEntry,
    isSelected: Boolean = false,
    onClick: (String) -> Unit = {}
) {
    Card(
        modifier
            .fillMaxWidth()
            .border(
                4.dp,
                if (isSelected) MaterialTheme.colors.error else Color.Transparent,
                RoundedCornerShape(50)
            )
            .height(50.dp)
            .clip(MaterialTheme.shapes.medium)
            .clickable { onClick(symbol) }
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 6.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.weight(0.9F)
            ) {
                Image(
                    painter = rememberImagePainter(item.stock?.logo),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(horizontal = 12.dp)
                        .size(32.dp)
                        .clip(RoundedCornerShape(50))
                )
                Text(
                    text = item.stock?.name ?: "",
                    fontWeight = if (isSelected) FontWeight.Bold else FontWeight.Normal
                )
            }
            Column(
                modifier = Modifier
                    .padding(end = 16.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.End
            ) {
                Text(
                    text = "${item.currentPrice?.let { formatCurrency(item.amount?.times(it)) }}",
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
                Text(
                    text = "${item.amount?.toInt()} $symbol",
                    fontSize = 12.sp,
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
            }
        }
    }
}

// Shows a item similar to the depot item but with your current balance
@Composable
fun BalanceItem(
    modifier: Modifier = Modifier,
    balance: Double?,
    isSelected: Boolean = false,
    onClick: () -> Unit = {}
) {
    Card(
        modifier
            .fillMaxWidth()
            .border(
                4.dp,
                if (isSelected) MaterialTheme.colors.error else Color.Transparent,
                RoundedCornerShape(50)
            )
            .height(50.dp)
            .clip(MaterialTheme.shapes.medium)
            .clickable { onClick() }
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 6.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.weight(0.9F)
            ) {
                Icon(
                    imageVector = Icons.Outlined.Paid,
                    contentDescription = null,
                    tint = MaterialTheme.colors.secondaryVariant,
                    modifier = Modifier
                        .padding(horizontal = 12.dp)
                        .size(32.dp)
                        .clip(RoundedCornerShape(50))
                )
                Text(
                    text = "Balance",
                    fontWeight = if (isSelected) FontWeight.Bold else FontWeight.Normal
                )
            }
            Column(
                modifier = Modifier
                    .padding(end = 16.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.End
            ) {
                Text(
                    text = "${balance?.let { formatCurrency(it) }}",
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
            }
        }
    }
}
